var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	ReportComment = require('../objects/reportcomment'),
	Report = require('../objects/report');

	User = new User();
	ReportComment = new ReportComment();
	Report = new Report();
		
	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {parent: false};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(body.report && ObjectID.isValid(body.report))
			find.report = new ObjectID(body.report);
		
		if(body.user && ObjectID.isValid(body.user))
			find.user = new ObjectID(body.user);
			
			
		return ReportComment.count(body, function(success, count){
			if(success)
				if(count>0){
					return ReportComment.getMultiFull(body,{ time:-1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
router.route('/id/:id')
	.get(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;
			
			
		return ReportComment.getFull(id, function(success, mes){
			if(success){
				return res.json({e:0, item: mes});
			}else
				return res.json({e:2, mes: mes});
		});	
	})
	.put(function(req,res){
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;

		if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
			if(ObjectID.isValid(req.params.id)){
				id = new ObjectID(id);
				var find = {_id: id};
				
				ReportComment.findOne(find, function(success, reportcomment){
					if(success){
						return ReportComment.doUpdate(id, body, function(success, mes){
							if(success)
								return res.json({e:0});
							else
								return res.json({e:1,mes:mes});
						});
					}else
						return res.json({e:2, mes: reportcomment});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
		
		if(ObjectID.isValid(id)){
		
			id = new ObjectID(id);
			
			if(req.user && (User.isAdmin(req.user) || User.isMod(req.user)))
				return ReportComment.remove({_id:id}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});

router.route('/')
	.post(function(req,res){
		var body = req.body;

		if(req.user){
			body.user = req.user._id;
			return ReportComment.insert(body, function(success, mes){
				if(success)
					return res.json({e:0, id: mes});
				else
					return res.json({e:1, mes: mes});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	 
module.exports = router;