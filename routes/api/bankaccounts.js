var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize');
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {parent: false};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(body.bank && ObjectID.isValid(body.bank))
			find.bank = new ObjectID(body.bank);
		
		if(body.user && ObjectID.isValid(body.user))
			find.user = new ObjectID(body.user);
			
		if(body.me && req.user)
			find.user = new ObjectID(req.user._id);
			
		return Bankaccount.count(body, function(success, count){
			if(success)
				if(count>0){
					return Bankaccount.getMultiFull(body,{ time:-1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
router.route('/id/:id')
	.get(function(req,res){
		var id = req.params.id;
			
			
		return Bankaccount.getFull(id, function(success, mes){
			if(success){
				return res.json({e:0, item: mes});
			}else
				return res.json({e:2, mes: mes});
		});	
	})
	.put(function(req,res){
		var body = req.body;

		if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
			if(ObjectID.isValid(req.params.id)){
				id = new ObjectID(req.params.id);
				var find = {_id: id};
				
				Bankaccount.findOne(find, function(success, bankaccount){
					if(success){
						return Bankaccount.doUpdate(id, body, function(success, mes){
							if(success)
								return res.json({e:0});
							else
								return res.json({e:1,mes:mes});
						});
					}else
						return res.json({e:2, mes: bankaccount});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
		
		if(ObjectID.isValid(id)){
		
			id = new ObjectID(id);
			
			if(req.user && (User.isAdmin(req.user) || User.isMod(req.user)))
				return Bankaccount.remove({_id:id}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});

router.route('/')
	.post(function(req,res){
		var body = req.body;

		if(req.user){
			body.user = req.user._id;
			return Bankaccount.insert(body, function(success, mes){
				if(success)
					return res.json({e:0, id: mes});
				else
					return res.json({e:1, mes: mes});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	 
module.exports = router;


var	User = require('../objects/user'),
	Bankaccount = require('../objects/bankaccount');

	User = new User();
	Bankaccount = new Bankaccount();