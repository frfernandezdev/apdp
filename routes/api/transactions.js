var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	request = require('request'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	Transaction = require('../objects/transaction');

	Transaction = new Transaction();
		
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			order = {},
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		
		if(body.orderDate){
			order.added = parseInt(body.added);
		}
		
		if(req.user){
			return Transaction.count(body, function(success, count){
				if(success)
					if(count>0){
						return Transaction.getMultiFull(body, order, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	});

router.route('/addDelivery/:id')
	.put(function(req,res){
		if(req.user){
			let id = req.params.id,
				body = req.body;
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				return Transaction.findOne({_id: id, buyer: new ObjectID(req.user._id)}, function(success, trans){
					if(success){
						if(body.idp && body.id && body.name && body.agency && body.address){
							let data = {
								idp: body.idp,
								id: body.id,
								name: body.name,
								agency: body.agency,
								address: body.address
							};
							
							return Transaction.update({_id: id}, {"$set":{billing: data}}, {}, function(success, mes){
								if(success){
									return res.json({e:0});
								}else
									return res.json({e:1, mes:mes});
							});
						}else{
							return res.json({e:2, mes:"Debes enviar todos los campos"});
						}
					}else{
						return res.json({e:1, mes:trans});
					}
				});
			}else{
				return res.json({e:1, mes:"ID Invalido"});
			}
		}else{
			return res.json({e:1, mes:"Token invalido"});
		}
		
	});

router.route('/addBilling/:id')
	.put(function(req,res){
		if(req.user){
			let id = req.params.id,
				body = req.body;
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				return Transaction.findOne({_id: id, buyer: new ObjectID(req.user._id)}, function(success, trans){
					if(success){
						if(body.idp && body.id && body.name && body.address){
							let data = {
								idp: body.idp,
								id: body.id,
								name: body.name,
								address: body.address
							};
							
							return Transaction.update({_id: id}, {"$set":{delivery: data}}, {}, function(success, mes){
								if(success){
									return res.json({e:0});
								}else
									return res.json({e:1, mes:mes});
							});
						}else{
							return res.json({e:2, mes:"Debes enviar todos los campos"});
						}
					}else{
						return res.json({e:1, mes:trans});
					}
				});
			}else{
				return res.json({e:1, mes:"ID Invalido"});
			}
		}else{
			return res.json({e:1, mes:"Token invalido"});
		}
		
	});

router.route('/addPayment/:id')
	.put(upload.single('file'), function(req, res){
		let id = req.params.id,
			body = req.body;
		
		if(!req.file)
			return next();
			
		return fileUtils.saveFile([req.file], "image", function(files){
			if(files){
				body.photo = files[0];
				
				if(ObjectID.isValid(id)){
					id = new ObjectID(id);
					return Transaction.findOne({_id: id, buyer: new ObjectID(req.user._id)}, function(success, trans){
						if(success){
							if(body.date && body.type && body.sum && body.bank && body.ref){
								let data = {
									date: body.date,
									type: body.type,
									sum: body.sum,
									ref: body.ref,
									bank: body.bank,
									photo: body.photo
								};
								
								return Transaction.update({_id: id}, {"$push":{payments: data}}, {}, function(success, mes){
									if(success){
										return res.json({e:0});
									}else
										return res.json({e:1, mes:mes});
								});
							}else{
								return res.json({e:2, mes:"Debes enviar todos los campos"});
							}
						}else{
							return res.json({e:1, mes:trans});
						}
					});
				}else{
					return res.json({e:1, mes:"ID Invalido"});
				}
			}else
				return res.json({e:3, mes:"Error al guardar archivo"});
		});	
		
	})
	.put(function(req,res){
		if(req.user){
			let id = req.params.id,
				body = req.body;
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				return Transaction.findOne({_id: id, buyer: new ObjectID(req.user._id)}, function(success, trans){
					if(success){
						if(body.date && body.type && body.sum && body.bank && body.ref){
							let data = {
								date: body.date,
								type: body.type,
								sum: body.sum,
								ref: body.ref,
								bank: body.bank
							};
							
							return Transaction.update({_id: id}, {"$push":{payments: data}}, {}, function(success, mes){
								if(success){
									return res.json({e:0});
								}else
									return res.json({e:1, mes:mes});
							});
						}else{
							return res.json({e:2, mes:"Debes enviar todos los campos"});
						}
					}else{
						return res.json({e:1, mes:trans});
					}
				});
			}else{
				return res.json({e:1, mes:"ID Invalido"});
			}
		}else{
			return res.json({e:1, mes:"Token invalido"});
		}
		
	});
	
router.route('/sales')
	.get(function(req, res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			body.seller = new ObjectID(req.user._id)
			return Transaction.count(body, function(success, count){
				if(success)
					if(count>0){
						return Transaction.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});
	
router.route('/purchases')
	.get(function(req, res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			body.buyer = new ObjectID(req.user._id)
			return Transaction.count(body, function(success, count){
				if(success)
					if(count>0){
						return Transaction.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});
	
	
router.route('/id/:id')
	.get(function(req,res){
		var id = req.params.id;
			
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			return Transaction.getFull(id, function(success, user){
				if(success){
					res.json({e:0, item: user});
				}else
					return res.json({e:2, mes: user});
			});	
		}else
			return res.json({e:2, mes: "ID Invalido"});
	});
		
	
router.route('/buy/:id')
	.post(function(req, res){
		var body = req.body;
		
		if(req.user){
			body.buyer = req.user._id;
			body.publication = req.params.id;
			
			return Transaction.insert(body, function(success, mes){
				console.log(arguments);
				if(success)
					return res.json({e:0, id: mes});
				else
					return res.json({e:2, mes: mes});
			});
		
		}else
			return res.json({e:1, mes: 'Token invalido'});
	});	 
module.exports = router;