var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Friendship = require('../objects/friendship'),
	Friendrequest = require('../objects/friendrequest');

	User = new User();
	Friendrequest = new Friendrequest();
	Friendship = new Friendship();

router.use(sanetize());	
	
router.route('/toMe')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			find.to = new ObjectID(req.user._id);
			return Friendrequest.count(find, function(success, count){
				if(success){
					return Friendrequest.getMultiFull(find, {_id: -1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	
router.route('/fromMe')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			find.from = new ObjectID(req.user._id);
			return Friendrequest.count(find, function(success, count){
				if(success){
					return Friendrequest.getMultiFull(find, {_id: -1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	
router.route('/accept/:id')
	.post(function(req, res){
		if(ObjectID.isValid(req.params.id)){
			if(req.user){
				var id = new ObjectID(req.params.id);
				return Friendrequest.findOne({_id:id, to: new ObjectID(req.user._id)}, function(success, mes){
					if(success){
						return Friendship.insert({
							users: [new ObjectID(mes.to), new ObjectID(mes.from)]
						}, function(success, mes){
							if(success){
								Friendrequest.remove({_id:id});
								return res.json({e:0});
							}else
								return res.json({e:4, mes: mes});
						});
					}else
						return res.json({e:3, mes: mes});
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		
		}else
			return res.json({e:1,mes:"ID invalido"});
	});		

router.route('/cancel/:id')
	.delete(function(req, res){
		if(ObjectID.isValid(req.params.id)){
			if(req.user){
				var id = new ObjectID(req.params.id);
				return Friendrequest.findOne({_id:id, "$or": [{from: new ObjectID(req.user._id)}, {to: new ObjectID(req.user._id)}]}, function(success, mes){
					if(success)
						return Friendrequest.remove({_id:id}, function(success, mes){
							if(success)
								return res.json({e:0});
							else
								return res.json({e:4, mes: mes});
						});
					else
						return res.json({e:3, mes: mes});
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		
		}else
			return res.json({e:1,mes:"ID invalido"});
	});		

router.route('/request/:id')
	.post(function(req, res){
		var body = req.body;
		
		if(ObjectID.isValid(req.params.id)){
			if(req.user){
				body.from = new ObjectID(req.user._id);
				body.to = new ObjectID(req.params.id);
			
			
				return Friendrequest.insert(body, function(success, mes){
					if(success)
						return res.json({e:0, id: mes});
					else
						return res.json({e:1, mes: mes});
				});
			}else
				return res.json({e:1,mes:"Token invalido"});
		
		}else
			return res.json({e:1,mes:"ID invalido"});
	});	
module.exports = router;