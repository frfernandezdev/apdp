var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	Categoryfaq = require('../objects/categoryfaq'),
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user');

	User = new User();
	Categoryfaq = new Categoryfaq();
	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 999999,
			term = body.term && body.term.length > 0 ? body.term : false ;
			
		start = parseInt(start);
		limit = parseInt(limit);
		var find = {};
		if(term)
			find.name = {"$regex":new RegExp(term,'i')};
		if(body.parent)
			find.parent = ObjectID.isValid(body.parent) ? new ObjectID(body.parent) : false;
			
		Categoryfaq.find(find,{searchName:1}, start, limit, function(success, categoriesfaq){
			if(success){
				return res.json({e:0,items:categoriesfaq});
			}else
				return res.json({e:1,mes:categoriesfaq});
		});
		return;
	});
	
router.route('/id/:id')
	.get(function(req,res){
		if(ObjectID.isValid(req.params.id)){
			Categoryfaq.findOne({_id: new ObjectID(req.params.id)}, function(success, categoryfaq){
				if(success) 
					res.json({e:0, item: categoryfaq});
				else
					res.json({e:2, mes: categoryfaq});
			});
		}else
			res.json({e:1, mes:"ID invalido"});
		return;
	})
	.put(function(req,res){
		var body = req.body,
			id = req.params.id;
			
		if(req.user && User.isAdmin(req.user)){
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				
				return Categoryfaq.findOne({_id:id}, function(success, cat){
					if(success){
						return Categoryfaq.doUpdate(id, body, function(success, result){
							if(success)
								return res.json({e:0,id:result});
							else
								return res.json({e:3,mes:result});
						});	
					}else
						return res.json({e:3, mes:cat});
				});
			}else
				return res.json({e:1,mes:"ID invalido"});	
		}else
			return res.json({e:2,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
	
		if(ObjectID.isValid(id)){
			if(req.user && User.isAdmin(req.user)){
				Categoryfaq.remove({_id:new ObjectID(id)}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else return res.json({e:3, mes:mes});
				
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		}else
			return res.json({e:1,mes:"ID invalido"});
	});
	
router.route('/')
	.post(function(req,res){
		var body = req.body;
		console.log(req.body);
		if(!body.name){
			return res.json({
				e:7,
				mes:"Debes enviar los campos requeridos"
			});
		}
		
		if(req.user && User.isAdmin(req.user)){
			return Categoryfaq.insert(body, function(success, result){
				if(success)
					return res.json({e:0,id:result});
				else
					return res.json({e:3,mes:result});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});	
	 
module.exports = router;