var express = require('express'),
	router = express.Router(),
	request = require('request'),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user');
	
	User = new User();
	
router.use(sanetize());	
	
router.route('/loginadmin')
	.post(function(req,res){
		var mailregex = new RegExp(["^", req.body.mail, "$"].join(""), "i");
		User.findOne({mail: mailregex}, function(success, user){
			if(success && (User.isAdmin(user) || User.isMod(user))){
					
				var password = user.password,
					bitacora = User.getBitacora();
			
				User.compareHashPass(password,req.body.password, function(isMatch){
					if(isMatch){
						User.update({_id:new ObjectID(user._id)},{"$push":{bitacora:bitacora}},{},function(success){
							if (success) {
								res.json({e:0,token:bitacora.token,tokenSecret:bitacora.tokenSecret, expiresAt: bitacora.expires});
							}else {
								res.json({e:2,mes:"Error al guardar token"});
							}
						});
					}else{
						return setTimeout(function(){
							return res.json({e:1,mes:"Email o Contraseña incorrecta"});
						}, 2000);
					}
				});
			
			}else
				return setTimeout(function(){
					return res.json({e:1,mes:"Email o Contraseña incorrecta"});
				}, 2000);
		});
	});

router.route('/login')
	.post(function(req,res){
		var or = {
			"$or": [
				{username: new RegExp(["^", req.body.mail, "$"].join(""), "i")},
				{mail: new RegExp(["^", req.body.mail, "$"].join(""), "i")},
			]
		};
		
		User.findOne(or, function(success, user){
			if(user){
				
				if(user.valid){
				
					var password = user.password,
						bitacora = User.getBitacora();
				
					User.compareHashPass(password,req.body.password, function(isMatch){
						if(isMatch){
							User.update({_id:new ObjectID(user._id)},{$push:{bitacora:bitacora}},{},function(success){
								if (success) {
									res.json({e:0,token:bitacora.token,tokenSecret:bitacora.tokenSecret, expiresAt: bitacora.expires});
								}else {
									res.json({e:2,mes:"Error al guardar token"});
								}
							});
						}else{
							res.json({e:1,mes:"Email o Contraseña incorrecta"});  
						}
					});
					
				}else
					res.json({e:1, mes: "Debes validar tu cuenta"});
			}else
				res.json({e:1, mes:"Email o Contraseña incorrecta"});
		});
	});                                                                                                    
	
router.route('/google')
	.post(function(req, res){
		console.log(req.body);
		var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token',
			peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect',
			params = {
				code: req.body.code,
				client_id: req.body.clientId,
				client_secret: "Q-XV-vVbI4Z9OgqE5Q6VkMl_",
				redirect_uri: req.body.redirectUri,
				grant_type: 'authorization_code'
			};

		request.post(accessTokenUrl, { json: true, form: params }, function(err, response, token) {
			var accessToken = token.access_token;
			var headers = { Authorization: 'Bearer ' + accessToken };

			
			request.get({ url: peopleApiUrl, headers: headers, json: true }, function(err, response, profile) {
				if (profile.error) {
					return res.status(500).send({message: "Error de comunicación con Google"});
				}
				
				profile.picture = profile.picture.replace('sz=50', 'sz=640');
				User.findOne({ "google.sub": profile.sub }, function(success, user) {
					if(success){
						var bitacora = User.getBitacora();
						User.update({_id:new ObjectID(user._id)}, {"$push":{bitacora:bitacora}, "$set":{google: profile}},{},function(success){
							if (success) {
								res.json({e:0, login: true, token:bitacora.token,tokenSecret:bitacora.tokenSecret, expiresAt: bitacora.expires});
							}else {
								res.json({e:2,mes:"Error al guardar token"});
							}
						});
					}else{
						profile = {
							first_name: profile.given_name,
							last_name: profile.family_name,
							picture: profile.picture.replace('sz=50', 'sz=640'),
							email: profile.email,
							sub: profile.sub,
							gender: profile.gender,
							access_token: accessToken,
							datasource: 'google'
						};
						return res.json({e:0, login: false, profile: profile});
					}
				});
			});
		});
	
	});
	
router.route('/facebook')
    .post(function(req,res){
	
		var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name', 'gender', 'picture.width(640)'],
			accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token',
			graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(','),
			params = {
			code: req.body.code,
			client_id: req.body.clientId,
			client_secret: "aa7d5e09d8091c6077a4e1a8c329709b",
			redirect_uri: req.body.redirectUri
		};
		
        
		
		request.get({ url: accessTokenUrl, qs: params, json: true }, function(err, response, accessToken) {
			if (response.statusCode !== 200) {
				return res.status(500).send({message: "Error de comunicación con Facebook"});
			}
			
			request.get({ url: graphApiUrl, qs: accessToken, json: true }, function(err, response, profile) {
			
				if (response.statusCode !== 200) {
					return res.status(500).send({message: "Error de comunicación con Facebook"});
				}
				
				profile.picture = profile.picture.data.url;
				profile.access_token = accessToken.access_token;
				User.findOne({ "facebook.id": profile.id }, function(success, user) {
					if(success){
						var bitacora = User.getBitacora();
						User.update({_id:new ObjectID(user._id)}, {"$push":{bitacora:bitacora}, "$set":{facebook: profile}},{},function(success){
							if (success) {
								res.json({e:0, login: true, token:bitacora.token,tokenSecret:bitacora.tokenSecret, expiresAt: bitacora.expires});
							}else {
								res.json({e:2,mes:"Error al guardar token"});
							}
						});
					}else{
						profile.datasource = "fb";
						return res.json({e:0, login: false, profile: profile});
					}
				});
			});
		});
    });		
	
module.exports = router;