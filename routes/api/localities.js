var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	Locality = require('../objects/locality'),
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user');
	
	
	User = new User();
	Locality = new Locality();
	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 999999;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		var find = {};
		
		if(body.name)
			find.name = {"$regex":new RegExp(body.name,'i')};
		
		if(body.parent)
			find.parent = ObjectID.isValid(body.parent) ? new ObjectID(body.parent) : false;
		
		
		Locality.count(find, function(success, count){
			if(success){
				if(count > 0)	
					Locality.find(find,{name:1}, start, limit, function(success, localities){
						if(success){
							return res.json({e:0,items:localities, total: count});
						}else
							return res.json({e:1,mes:localities});
					});
				else
					return res.json({e:0,items:[], total: 0});
			}else
				return res.json({e:1,mes:count});
		});
		return;
	});


router.route('/whereis/:lat/:lng')
	.get(function(req,res){
		Locality.find(
			{
				geometry:{
					"$geoIntersects": { 
						"$geometry": { 
							type: "Point", 
							coordinates: [ parseFloat(req.params.lng), parseFloat(lat) ] 
						} 
					} 
				}
			},
			{area: 1},
			0,
			1,
			function(success, items){
				if(success && items.length > 0){
					return res.json({e:0, item: items[0]});
				}else{
					return res.json({e:1, mes: 'No se ha encontrado tu ubicación en nuestra base'});
				}
			});
		
	});
	
router.route('/id/:id')
	.get(function(req,res){
		if(ObjectID.isValid(req.params.id)){
			Locality.findOne({_id: new ObjectID(req.params.id)}, function(success, locality){
				if(success) 
					res.json({e:0, item: locality});
				else
					res.json({e:2, mes: locality});
			});
		}else
			res.json({e:1, mes:"ID invalido"});
		return;
	})
	.put(function(req,res){
		var body = req.body,
			id = req.params.id;
			
		if(req.user && User.isAdmin(req.user)){
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				
				return Locality.findOne({_id:id}, function(success, locality){
					if(success){
						return Locality.doUpdate(id, body, function(success, result){
							if(success)
								return res.json({e:0,id:result});
							else
								return res.json({e:3,mes:result});
						});	
					}else
						return res.json({e:3, mes:locality});
				});
			}else
				return res.json({e:1,mes:"ID invalido"});	
		}else
			return res.json({e:2,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
		if(ObjectID.isValid(id)){
			if(req.user && User.isAdmin(req.user)){
				Locality.remove({_id:new ObjectID(id)}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else return res.json({e:3, mes:mes});
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		}else
			return res.json({e:1,mes:"ID invalido"});
	});
	
router.route('/')
	.post(function(req,res){
		var body = req.body;
		
		if(req.user && User.isAdmin(req.user)){
			return Locality.insert(body, function(success, result){
				if(success)
					return res.json({e:0,id:result});
				else
					return res.json({e:3,mes:result});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});	
	 
module.exports = router;