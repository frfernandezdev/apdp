var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Faq = require('../objects/faq');

	User = new User();
	Faq = new Faq();

router.use(sanetize({
	allowedTags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'center', 'h6', 'blockquote', 'p', 'ul', 'ol', 'font', 
		'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
		'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'img'],
	allowedAttributes: {
		h1: ['style'],
		h2: ['style'],
		h3: ['style'],
		h4: ['style'],
		h5: ['style'],
		h6: ['style'],
		center: ['style'],
		h6: ['style'],
		blockquote: ['style'],
		p: ['style'],
		a: ['style'],
		ul: ['style'],
		ol: ['style'],
		font: ['style'],
		nl: ['style'],
		li: ['style'],
		b: ['style'],
		i: ['style'],
		strong: ['style'],
		em: ['style'],
		strike: ['style'],
		code: ['style'],
		hr: ['style'],
		br: ['style'],
		div: ['style'],
		table: ['style'],
		thead: ['style'],
		caption: ['style'],
		tbody: ['style'],
		tr: ['style'],
		th: ['style'],
		td: ['style'],
		pre: ['style'],
		img: ['src', 'alt', 'style']
	}
 }));	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		return Faq.count(body, function(success, count){
			if(success)
				if(count>0){
					return Faq.getMultiFull(body,{}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
router.route('/id/:id')
	.get(function(req,res){
		var id = req.params.id;
			
			
		return Faq.getFull(id, function(success, mes){
			if(success){
				if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
					if(!new ObjectID(req.user && (User.isAdmin(req.user) || User.isMod(req.user))._id).equals(mes.user._id))
						makeView(req.user && (User.isAdmin(req.user) || User.isMod(req.user)), mes);
				}else
					makeView(false, mes);
				return res.json({e:0, item: mes});
			}else
				return res.json({e:2, mes: mes});
		});	
	})
	.put(function(req,res){
		var body = req.body;

		if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
			if(ObjectID.isValid(req.params.id)){
				id = new ObjectID(req.params.id);
				var find = {_id: id};
				
				Faq.findOne(find, function(success, faq){
					if(success){
						return Faq.doUpdate(req.user, id, body, function(success, mes){
							if(success)
								return res.json({e:0});
							else
								return res.json({e:1,mes:mes});
						});
					}else
						return res.json({e:2, mes: faq});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
		
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			if(req.user && (User.isAdmin(req.user) || User.isMod(req.user)))
				return Faq.remove({_id:id}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});

router.route('/')
	.post(function(req,res){
		var body = req.body;
	
		if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
			return Faq.insert(body, function(success, mes){
				if(success)
					return res.json({e:0, id: mes});
				else
					return res.json({e:1, mes: mes});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	 
module.exports = router;