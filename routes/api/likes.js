var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Like = require('../objects/like'),
	Publication = require('../objects/publication');

	User = new User();
	Like = new Like();
	Publication = new Publication();
		
	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {parent: false};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(body.publication && ObjectID.isValid(body.publication))
			find.publication = new ObjectID(body.publication);
		
		if(body.user && ObjectID.isValid(body.user))
			find.user = new ObjectID(body.user);
			
		if(req.user && body.byMe)
			find.user = new ObjectID(req.user._id);
			
		return Like.count(body, function(success, count){
			if(success)
				if(count>0){
					return Like.getMultiFull(body,{ time:-1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
router.route('/id/:id')
	.get(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;
			
			
		return Like.getFull(id, function(success, mes){
			if(success){
				return res.json({e:0, item: mes});
			}else
				return res.json({e:2, mes: mes});
		});	
	})
	.put(function(req,res){
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;

		if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
			if(ObjectID.isValid(req.params.id)){
				id = new ObjectID(id);
				var find = {_id: id};
				
				Like.findOne(find, function(success, like){
					if(success){
						return Like.doUpdate(id, body, function(success, mes){
							if(success)
								return res.json({e:0});
							else
								return res.json({e:1,mes:mes});
						});
					}else
						return res.json({e:2, mes: like});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
		
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			if(req.user)
				return Like.remove({_id:id, user: new ObjectID(req.user._id)}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});

router.route('/')
	.post(function(req,res){
		var body = req.body;

		if(req.user){
			body.user = req.user._id;
			return Like.insert(body, function(success, mes){
				if(success)
					return res.json({e:0, id: mes});
				else
					return res.json({e:1, mes: mes});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	 
module.exports = router;