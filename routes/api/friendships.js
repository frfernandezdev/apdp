var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Friendship = require('../objects/friendship'),
	Friendrequest = require('../objects/friendrequest');

	User = new User();
	Friendrequest = new Friendrequest();
	Friendship = new Friendship();

router.use(sanetize());	
		
router.route('/id/:id')
	.delete(function(req, res){
		if(req.user){
			if(ObjectID.isValid(req.params.id)){
		
				var find = {_id: new ObjectID(req.params.id), users: new ObjectID(req.user._id)};
				return Friendship.findOne(find, function(success, count){
					if(success){
						return Friendship.remove(find, function(success, items){
							if(success)
								return res.json({e:0});
							else
								return res.json({e:3, mes:items});
						});
					}else
						return res.json({e:2, mes:count});
				});
			
			}else
				return res.json({e:1, mes:"ID invalido"});
				
			
		}else
			return res.json({e:1, mes:"Token invalido"});
	});
		
router.route('/user/:id')
	.get(function(req, res){
		if(req.user){
			var body = req.query,
				limit = body.limit || 20,
				start = body.start || 0;
			
			if(ObjectID.isValid(req.params.id)){
				var find = {users: new ObjectID(req.params.id)};
				return Friendship.count(find, function(success, count){
					if(success){
						if(count > 0)
							return Friendship.find(find, {}, start, limit, function(success, items){
								if(success)
									return res.json({e:0, items:items, total:count});
								else
									return res.json({e:3, mes:items});
							});
						else
							return res.json({e:0, items:[], total:0});
					}else
						return res.json({e:2, mes:count});
				});
			}else{
				return res.json({e:1, mes:"ID invalido"});
			}
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});
			
router.route('/')
	.get(function(req, res){
		if(req.user){
			var body = req.query,
				limit = body.limit || 20,
				start = body.start || 0,
				find = {users: new ObjectID(req.user._id)};
				
			return Friendship.count(find, function(success, count){
				if(success){
					if(count > 0)
						return Friendship.find(find, {}, start, limit, function(success, items){
							if(success)
								return res.json({e:0, items:items, total:count});
							else
								return res.json({e:3, mes:items});
						});
					else
						return res.json({e:0, items:[], total:0});
				}else
					return res.json({e:2, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});
	
module.exports = router;