var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	request = require('request'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	Review = require('../objects/review');

	Review = new Review();

router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		return Review.count(body, function(success, count){
			if(success)
				if(count>0){
					return Review.getMultiFull(body,{}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
		
	});
	
	
router.route('/forme')
	.get(function(req, res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			body.to = new ObjectID(req.user._id)
			return Review.count(body, function(success, count){
				if(success)
					if(count>0){
						return Review.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});

router.route('/byme')
	.get(function(req, res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			body.by = new ObjectID(req.user._id)
			return Review.count(body, function(success, count){
				if(success)
					if(count>0){
						return Review.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});	
	
router.route('/review/:transaction')
	.post(function(req,res){
		var body = req.body;
		if(req.user){
			if(ObjectID.isValid(req.params.transaction)){
				body.transaction = req.params.transaction;
				body.by = req.user._id;
				return Review.insert(body, function(success, mes){
					if(success){
						return res.json({e:0, id: mes});
					}else
						return res.json({e:2, mes: mes});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});

 
module.exports = router;