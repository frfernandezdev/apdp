var express = require('express'),
	User = require('../objects/user'),
	ObjectID = require('mongoskin').ObjectID,
	Category = require('../objects/category'),
	router = express.Router();
	
	User = new User();
	Category = new Category();
	
router.get('/terminos', function(req,res){
	return res.render('bot', {
		url: req.protocol + '://' + req.get('host') + req.originalUrl,
		title: "Delivery En Bahia | Términos y Condiciones",
		description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
		image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
		keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery",
		fb_app_id: "1853944701597868"
	});
});	

router.get('/privacidad', function(req,res){
	return res.render('bot', {
		url: req.protocol + '://' + req.get('host') + req.originalUrl,
		title: "Delivery En Bahia | Politica de Privacidad",
		description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
		image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
		keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery",
		fb_app_id: "1853944701597868"
	});
});	

router.get('/casadecomida/:name/:id', function(req,res,next){
	if(ObjectID.isValid(req.params.id)){
		let id = new ObjectID(req.params.id);
		User.getFull(id, function(success, user){
			if(success && User.isHouse(user)){
				let catnames = [], sinonims = [];
				for(let i = 0 ; i < user.categories.length ; i++){
					catnames.push(user.categories[i].name);
					sinonims.push(user.categories[i].sinonims);
				}
				
				return res.render('bot', {
					url: req.protocol + '://' + req.get('host') + req.originalUrl,
					title: "Delivery En Bahia | "+user.name,
					description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
					image: user.plan >= 2 ? "https://uploads.deliveryenbahia.com/200/"+user.photo : "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
					keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery,"+user.name+","+catnames.join(",")+","+sinonims.join(","),
					fb_app_id: "1853944701597868"
				});
			}else
				return next("route");
		});
	}else
		return next("route");
});	

router.get('/login', function(req,res){
	return res.render('bot', {
		url: req.protocol + '://' + req.get('host') + req.originalUrl,
		title: "Delivery En Bahia |  Ingresa",
		description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
		image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
		keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery",
		fb_app_id: "1853944701597868"
	});
});	

router.get('/micuenta', function(req,res,next){
	next();
});	

router.get('/registro', function(req,res){
	return res.render('bot', {
		url: req.protocol + '://' + req.get('host') + req.originalUrl,
		title: "Delivery En Bahia |  Registrate",
		description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
		image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
		keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery",
		fb_app_id: "1853944701597868"
	})
});	

router.get('/categoria/:name/:category', function(req,res, next){
	if(ObjectID.isValid(req.params.category)){
		let id = new ObjectID(req.params.category);
		Category.findOne({_id:id}, function(success, item){
			if(success)
				return res.render('bot', {
					url: req.protocol + '://' + req.get('host') + req.originalUrl,
					title: "Delivery En Bahia | Busqueda de "+item.name,
					description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
					image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
					keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery,"+item.name+","+item.sinonims,
					fb_app_id: "1853944701597868"
				});
			else
				return next("route");
		});
	}else
		return next("route");
});	

router.get('/categoriacerca/:name/:category/:lat/:lng', function(req,res){
	if(ObjectID.isValid(req.params.category)){
		let id = new ObjectID(req.params.category);
		Category.findOne({_id:id}, function(success, item){
			if(success)
				return res.render('bot', {
					url: req.protocol + '://' + req.get('host') + req.originalUrl,
					title: "Delivery En Bahia | Busqueda de "+item.name,
					description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
					image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
					keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery,"+item.name+","+item.sinonims,
					fb_app_id: "1853944701597868"
				});
			else
				return next("route");
		});
	}else
		return next("route");
});	

router.get('/busqueda/:term', function(req,res){
	return res.render('bot', {
		url: req.protocol + '://' + req.get('host') + req.originalUrl,
		title: "DeliveryEnBahia",
		description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
		image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
		keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery",
		fb_app_id: "1853944701597868"
	});
});	

router.get('/busquedacerca/:term/:lat/:lng', function(req,res){
	return res.render('bot', {
		url: req.protocol + '://' + req.get('host') + req.originalUrl,
		title: "DeliveryEnBahia",
		description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
		image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
		keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery",
		fb_app_id: "1853944701597868"
	});
});	

router.get('/busquedaf/:name/:category/:term/:lat/:lng', function(req,res){
	if(ObjectID.isValid(req.params.category)){
		let id = new ObjectID(req.params.category);
		Category.findOne({_id:id}, function(success, item){
			if(success)
				return res.render('bot', {
					url: req.protocol + '://' + req.get('host') + req.originalUrl,
					title: "Delivery En Bahia | Busqueda de "+item.name,
					description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
					image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
					keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery,"+item.name+","+item.sinonims,
					fb_app_id: "1853944701597868"
				});
			else
				return next("route");
		});
	}else
		return next("route");
});	

router.get('/*', function(req,res){
	return res.render('bot', {
		url: req.protocol + '://' + req.get('host') + req.originalUrl,
		title: "DeliveryEnBahia",
		description: "Encontra toda la oferta gastronómica de la ciudad en delivery bahia blanca.",
		image: "https://uploads.deliveryenbahia.com/200/delilogored.jpg",
		keywords: "entregas,comida,bahia blanca,bahía blanca,argentina,delivery",
		fb_app_id: "1853944701597868"
	});
});	

module.exports = router;