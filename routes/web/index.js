var express = require('express'),
	router = express.Router();

router.get('/views/:name', function(req, res) {
    if(req.user){
		res.render('parciales/'+req.params.name,{user: user,logged:true});
    }else
		res.render('parciales/'+req.params.name,{logged:false,expired:false});
});

router.get('/*', function(req, res) {
    if(req.user){
        res.render('index',{user: user,logged:true});
    }else
		res.render('index',{logged:false,expired:false});
});

module.exports = router;