var db = require('../utils/db'),
	collection = db.collection("bankaccounts"),
	ObjectID = require('mongoskin').ObjectID,
	method = BankAccount.prototype;

function BankAccount(){
	
}

method.update = function(query, order, extra, cb){
	collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doUpdate = function(user, id, body, oldbank, oldnumber, cb){
	var data = {};
	
	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(id);
	
	if(body.bank || ObjectID.isValid(body.bank))
		data.bank = new ObjectID(body.bank);
		
	if(!isNaN(body.type) && parseInt(body.type) >= 1 && parseInt(body.type) <= 2)
		data.bank = parseInt(body.type)
		
	if(body.id)
		data.id = body.id;
	
	if(body.name)
		data.name = body.name;
		
	if(body.number)
		data.number = body.number;
		
	if(body.mail)
		data.mail = body.mail;

		
	if(Object.keys(data).length > 0){
		if(data.bank){
			return Bank.findOne({_id: data.bank}, function(success, bank){
				if(success){
					if(data.number)
						if(!data.number.test(new RegExp(bank.format)))
							return cb ? cb(false, "El número de cuenta no coincide con el formato del banco") : false;
					else		
						if(!oldnumber.test(new RegExp(bank.format)))
							return cb ? cb(false, "El número de cuenta no coincide con el formato del banco") : false;	
					
					return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
						if(success){
							return cb ? cb(true, data.bitacora[0]) : false;
						}else{
							return cb ? cb(false, mes) : false;
						}
					});
					
				}else
					return cb ? cb(false, bank) : false;
			});
		}else{
			if(data.number)
				if(!data.number.test(new RegExp(oldbank.format)))
					return cb ? cb(false, "El número de cuenta no coincide con el formato del banco") : false;
			return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
				if(success){
					return cb ? cb(true, data.bitacora[0]) : false;
				}else{
					return cb ? cb(false, mes) : false;
				}
			});
		}
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, bankaccounts){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(bankaccounts) return cb ? cb(true, bankaccounts) : false;
		else return cb ? cb(false, "Cuentas bancarias no encontrados") : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];
					
					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return Bank.findOne({_id: new ObjectID(item.bank)}, function(success, bank){
		if(success)
			item.bank = bank;
			
		return cb ? cb(true, user) : false;
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Cuenta Bancaria no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){
	if(!body.bank || !ObjectID.isValid(body.bank))
		return cb ? cb(false, "Debes enviar el banco al que pertenece esta cuenta") : false;
		
	if(isNaN(body.type) || parseInt(body.type) < 1 || parseInt(body.type) > 2)
		return cb ? cb(false, "Debes enviar un tipo valido de cuenta") : false;	
		
	if(!body.id)
		return cb ? cb(false, "Debes enviar el identificador del titular de la cuenta bancaria") : false;
	
	if(!body.name)
		return cb ? cb(false, "Debes enviar el nombre del titular de la cuenta bancaria") : false;
		
	if(!body.number)
		return cb ? cb(false, "Debes enviar el número de cuenta bancaria") : false;
		
	if(!body.mail)
		return cb ? cb(false, "Debes enviar el correo asociado a esta cuenta bancaria") : false;
		
	if(!ObjectID.isValid(body.user))
		return cb ? cb(false, "Debes enviar el usuario de esta cuenta bancaria") : false;
	
	var data = {
		bank: new ObjectID(body.bank),
		type: parseInt(body.type),
		id: body.id,
		name: body.name,
		number: body.number,
		mail: body.mail,
		user: new ObjectID(body.user)
	};	
		
	return User.findOne({_id: data.user}, function(success, user){
		if(success){
			return Bank.findOne(data.bank, function(success, bank){
				if(success){
					if(!data.number.test(new RegEx(bank.format)))
						return cb ? cb(false, "El número de cuenta no coincide con el formato del banco") : false;
				
					return collection.insert(data, function(err, result){
						if(err){
							console.log(err);
							return cb ? cb(false, "Error al agregar publicación") : false;
						}else{
							return cb ? cb(true, data.validateToken, new ObjectID(result.insertedIds[0])) : false;
						}
					});
				}else
					return cb ? cb(false, bank) : false;
			});
		}else
			return cb ? cb(false, user) : false;
	});
};

module.exports = BankAccount;


var Bank = require(__dirname +'/bank'),
	User = require(__dirname +'/user');

	Bank = new Bank();
	User = new User();