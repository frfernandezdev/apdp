var db = require('../utils/db'),
	collection = db.collection("banner"),
	ObjectID = require('mongoskin').ObjectID,
	method = Banner.prototype,
	fileUtils = require('../utils/fileUtils');

function Banner(){
}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	this.find(filter, false, false, false, function(items){
		if(items){
			for(var i = 0 ; i < items.length ; i++)
				fileUtils.deleteFile("routes/uploads/husk/"+items[i].file);
			
			return collection.remove(filter, function(err){
				if(err){
					console.log(err);
					return cb ? cb(false, "Error al eliminar") : false;
				}else
					return cb ? cb(true) : false;
			});
		}else
			return cb ? cb(false, "Nada por eliminar") : false;
	});
	return;
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
	return;
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false) : false;
		}else if(users)
			return cb ? cb(true, users) : false;
		else
			return cb ? cb(false, "Banner no encontrada") : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Banner no encontrada") : false;
	});
};

method.insert = function(data, cb){
	if(data.file){
		return collection.insert(data, function(err, result){
			if(err){
				fileUtils.deleteFile("routes/uploads/husk/"+data.file);
				return cb ? cb(false, "Error al insertar") : false;
			}else{
				return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
			}
		});
	}else{
		fileUtils.deleteFile("routes/uploads/husk/"+data.file);
		return cb ? cb(false, "Debes enviar todos los campos") : false;
	}
};

module.exports = Banner;