var db = require('../utils/db'),
	collection = db.collection("reportcomments"),
	ObjectID = require('mongoskin').ObjectID,
	method = ReportComment.prototype,
	Report = require(__dirname +'/report'),
	User = require(__dirname +'/user');

	User = new User();
	Report = new Report();

function ReportComment(){
	
}
/*
method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doUpdate = function(user, id, body, oldstatus, cb){
	var data = {};
	
	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(id);
	
	if(body.reportcomment)
		data.reportcomment = body.reportcomment;
		
	if(Object.keys(data).length > 0){
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				return cb ? cb(true, data.bitacora[0]) : false;
			}else{
				return cb ? cb(false, mes) : false;
			}
		});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;
};
method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

*/
method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, reportcomments){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(reportcomments) return cb ? cb(true, reportcomments) : false;
		else return cb ? cb(false, "Transacciones no encontradas") : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];
					
					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return User.getFull({_id: new ObjectID(item.user)}, function(success, user){
		if(success)
			item.user = user;
			
		return cb ? cb(true, user) : false;
	}.bind(this));
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Cuenta Bancaria no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){
	
	if(!body.report || !ObjectID.isValid(body.report))
		return cb ? cb(false, "Debes enviar a que reporte pertenece esta comentario") : false;
	
	if(!body.user || !ObjectID.isValid(body.user))
		return cb ? cb(false, "Debes enviar el usuario que comenta") : false;
	
	if(!body.reportcomment)
		return cb ? cb(false, "Debes enviar el comentario") : false;	
		
	var data = {
		report: new ObjectID(body.report),
		user: new ObjectID(body.user),
		comment: body.comment
	};	
	
		
	return User.findOne({_id: data.user}, function(success, user){
		if(success){
			return Report.findOne({_id: data.report, status:1}, function(success, report){
				if(success){
				
					if(User.isAdmin(user))
						data.bytype = 4;
					else if(User.isMod(user))
						data.bytype = 3;
					else if(data.user.equals(new ObjectID(report.openfor)))
						data.bytype = 2;
					else if(data.user.equals(new ObjectID(report.openby)))
						data.by = 1;
					else
						return cb ? cb(false, "No puedes comentar este reporte") : false;
				
					return collection.insert(data, function(err, result){
						if(err){
							console.log(err);
							return cb ? cb(false, "Error al registrar transacción") : false;
						}else{
							return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
						}
					});
				
				}else
					return cb ? cb(false, publication) : false;
			}.bind(this));
		}else
			return cb ? cb(false, user) : false;
	}.bind(this));
};

module.exports = ReportComment;