var db = require('../utils/db'),
	collection = db.collection("publications"),
	ObjectID = require('mongoskin').ObjectID,
	fileUtils = require('../utils/fileUtils'),
	mailer = require('../utils/mailer'),
	uuid = require('node-uuid'),
	Hashids = require("hashids"),
    hashids = new Hashids("apreciodepana"),
	method = Publication.prototype;

function Publication(){
	
}

method.update = function(query, order, extra, cb){
	collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.checkCategories = function(data, id, cb, oldpublication){
	if(data.categories){
		return Category.validateArray(data.categories, function(success, mes){
			if(success){
				return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
					if(success){
						if(data.photos)
							this.deleteFiles(oldpublication.photos);
						return cb ? cb(true) : false;
					}else{
						if(data.photos)
							this.deleteFiles(data.photos);
						return cb ? cb(false, mes) : false;
					}
				}.bind(this));
			}else{
				if(data.photos)
					this.deleteFiles(data.photos);
				return cb ? cb(false, mes) : false;
			}
		}.bind(this));
	}else
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				if(oldpublication)
					this.deleteFiles(oldpublication.photos);
				return cb ? cb(true) : false;
			}else{
				if(data.photos)
					this.deleteFiles(data.photos);
				return cb ? cb(false, mes) : false;
			}
		}.bind(this));
};

method.checkLocality = function(data, id, cb, oldpublication){
	if(data.localities){
		return Locality.validateArray(data.localities, function(success, mes){
			if(success){
				return this.checkCategories(data, id, cb, oldpublication);
			}else{
				if(data.photos)
					this.deleteFiles(data.photos);
				return cb ? cb(false, mes) : false;
			}
		}.bind(this));
	}else
		return this.checkCategories(data, id, cb, oldpublication);
};

method.doUpdate = function(user, body, oldpublication, cb){
	
	
	var data = {};
	
	if(!ObjectID.isValid(oldpublication._id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(oldpublication._id);
	
	if(body.name && body.name.length > 10 && body.name.length < 110){
		data.name = body.name;
		data.searchname = this.clean(body.name);
	}
	
	if(body.body)
		data.body = body.body;
	
	if(body.stock)
		data.stock = parseInt(body.stock);
		
	if(body.state)
		data.state = parseInt(body.state);
		
	if(body.status)
		data.status = parseInt(body.status);
		
	if(body.price)
		data.price = parseFloat(body.price);
			
	if(body.waranty)
		data.waranty = body.waranty == 'true';
		
	if(body.invoice)
		data.invoice = body.invoice == 'true';
		
	if(body.physical)
		data.physical = body.physical == 'true';
	
	if(body.onfacebook)
		data.onfacebook = body.onfacebook == 'true';

	if(body.ontwitter)
		data.ontwitter = body.ontwitter == 'true';

	if(body.onfanpage)
		data.onfanpage = body.onfanpage == 'true';

	if(body.startdate)	
		data.startdate = this.dateToTime(body.startdate);
	
	if(Array.isArray(body.localities)){
		let locs = [];
		for(let key in body.localities)
			if(ObjectID.isValid(body.localities[key]))
				locs.push(new ObjectID(body.localities[key]));
		data.localities = locs;
	}else if(ObjectID.isValid(body.localities))
		data.localities = [new ObjectID(body.localities)];
	
	if(Array.isArray(body.categories)){
		for(let key in body.categories)
			if(ObjectID.isValid(body.categories[key]))
				body.categories[key] = new ObjectID(body.categories[key]);
		data.categories = body.categories;
	}else if(ObjectID.isValid(body.categories))
		data.categories = [new ObjectID(body.categories)];
	
	if(Array.isArray(body.photos) &&  body.photos.length > 0)
		data.photos = body.photos;

	if(Object.keys(data).length > 0){
		return this.checkLocality(data, id, cb, oldpublication);
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;	
};

method.deleteFiles = function(files){
	for(let j = 0; j < files.length ; j++)
		fileUtils.deleteFile("routes/uploads/husk/"+files[j]);
};

method.remove = function(filter, cb){
	return this.find(filter, false, false, false, function(items){
		if(items){
			var handleLoop = function(i){
				this.deleteFiles(items[i].photos);
			}.bind(this);
			
			for(var i = 0 ; i < items.length ; i++)
				handleLoop(i);
			
			return collection.remove(filter, function(err){
				if(err){
					console.log(err);
					return cb ? cb(false, "Error al eliminar") : false;
				}else
					return cb ? cb(true) : false;
			});
		}else
			return cb ? cb(false, "Nada por eliminar") : false;
	}.bind(this));
};

method.count = function(filter, cb){
	filter = this.prepareFind(filter);
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};

method.kilometersToRadians = function(km){
    return km / 6371;
};	

method.prepareFind = function(body){
	var find = {};
	
	if(ObjectID.isValid(body._id))
		find._id = new ObjectID(body._id);
	
	if(body.name)
		find.searchname = {"$regex":new RegExp(this.clean(body.name),'i')};
		
	if(body.body)
		find.searchbody = {"$regex":new RegExp(this.clean(body.body),'i')};
		
	if(body.state)
		find.state = parseInt(body.state);
		
	if(Array.isArray(body.localities)){
		let locs = [];
		for(let key in body.localities)
			if(ObjectID.isValid(body.localities[key]))
				locs.push(new ObjectID(body.localities[key]));
		find.localities = locs;
	}else if(ObjectID.isValid(body.localities))
		find.localities = [new ObjectID(body.localities)];
	
	if(Array.isArray(body.categories)){
		let cats = [];
		for(let key in body.categories)
			if(ObjectID.isValid(body.categories[key]))
				cats.push(new ObjectID(body.categories[key]));
		find.categories = {"$in":body.cats};
	}else if(ObjectID.isValid(body.categories))
		find.categories = {"$in":[new ObjectID(body.categories)]};
	
	if(body.minprice){
		if(!find.price) find.price = {};
		find.price["$gte"] = parseFloat(body.minprice);
	}	
	
	if(body.maxprice){
		if(!find.price) find.price = {};
		find.price["$lte"] = parseFloat(body.maxprice);
	}	
		
	if(body.lat && body.lng){
		var lat = parseFloat(body.lat),
			lng = parseFloat(body.lng);
		find.position = {
			"$geoWithin": {
				"$centerSphere" : [[lng,lat],this.kilometersToRadians( !body.distance || isNaN(body.distance) ? 1.5 : parseFloat(body.distance))]
			}
		};
	}

	if(ObjectID.isValid(body.user))
		find.user = new ObjectID(body.user);
		
	return find;
};

method.find = function(body, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var find = this.prepareFind(body),
		promise = collection.find(find);
		
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, publications){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(publications) return cb ? cb(true, publications) : false;
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(body, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var find = this.prepareFind(body),
		promise = collection.find(find);
		
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, publications){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(publications){
			var il = publications.length, round = 1, i = 0, total = 0, newpublications = [];
			var handleLoop = function(i){
				this.getFull(publications[i]._id, function(success, user){
					if(success)
						newpublications.push(user);
					else
						newpublications.push(publications[i]);
					
					if(round == il)
						return cb ? cb(true, newpublications) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, publications) : false;
		}
		else return cb ? cb(false, "Publicaciones no encontradas") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return Locality.validateArray(item.localities, function(success, localities){
		if(success)
			item.localities = localities;
			
		return Category.validateArray(item.categories, function(success, categories){
			if(success)
				item.categories = categories;
				
			return User.getFull(item.user, function(success, user){
				if(success)
					item.user = user;
				
				
				return Favorite.count({publication: item._id}, function(success, favorites){
					if(success)
						item.favorites = favorites;
					
					
					return Transaction.count({publication: item._id}, function(success, transactions){
						if(success)
							item.transactions = transactions;
					
						return View.count({publication: new ObjectID(item._id)}, function(success, views){
							if(success)
								item.views = views;
							return Like.count({publication: new ObjectID(item._id)}, function(success, likes){
								if(success)
									item.likes = likes;
								return cb ? cb(true, item) : false;
							});
						});
					});
					
					
				});
				
			});
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, user){
			if(success){
				return this.proccess(user, cb);
			}else
				return cb ? cb(false, "Publicación no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.str_replace = function($f, $r, $s){
    return $s.replace(new RegExp("(" + (typeof($f) == "string" ? $f.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&") : $f.map(function(i){return i.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&")}).join("|")) + ")", "g"), typeof($r) == "string" ? $r : typeof($f) == "string" ? $r[0] : function(i){ return $r[$f.indexOf(i)]});
};

method.clean = function(name){
	var f=[' ','Š','Œ','Ž','š','œ','ž','Ÿ','¥','µ','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','?','Ì','Í','Î','Ï','I','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','?','ì','í','î','ï','i','ð','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ'],
		r=['','S','O','Z','s','o','z','Y','Y','u','A','A','A','A','A','A','A','C','E','E','E','E','E','I','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','a','c','e','e','e','e','e','i','i','i','i','i','o','n','o','o','o','o','o','o','u','u','u','u','y','y'];
	return this.str_replace(f,r,name).toLowerCase();		
};

method.dateToTime = function(date){
	date=date.split("-");
	return new Date(date[2]+"-"+date[1]+"-"+date[0]).getTime();
}

method.insert = function(body, cb){
	
	if(!body.name)
		return cb ? cb(false, "Debes enviar el nombre de la publicación") : false;
		
	if(body.name.length < 10)
		return cb ? cb(false, "El nombre de la publicación debe tener al menos 10 caracteres") : false;	
		
	if(body.name.length > 110)
		return cb ? cb(false, "El nombre de la publicación no debe sobrepasar los 110 caracteres") : false;	
	
	if(!body.body)
		return cb ? cb(false, "Debes enviar la descripción de la publicación") : false;
	
	if(!body.stock)
		return cb ? cb(false, "Debes enviar el la cantidad de articulos en tu publicación") : false;
		
	if(!body.state)
		return cb ? cb(false, "Debes enviar el estado en tu publicación") : false;
		
	if(!body.price)
		return cb ? cb(false, "Debes enviar el precio de publicación") : false;
			
	if(!body.waranty)
		body.waranty = false;
		
	if(!body.invoice)
		body.invoice = false;
		
	if(!body.physical)
		body.physical = false;
	
	if(!body.onfacebook)
		body.onfacebook = false;

	if(!body.ontwitter)
		body.ontwitter = false;

	if(!body.onfanpage)
		body.onfanpage = false;

	if(!body.lat || !body.lng){
		body.lat = 0;
		body.lng = 0;
	}
	//return cb ? cb(false, "Debes enviar la ubicación de la publicación") : false;
		
	if(body.startdate)	
		body.startdate = this.dateToTime(body.startdate);
	else
		body.startdate = Date.now();
		
	if(!body.localities)
		return cb ? cb(false, "Debes enviar la ubicación de la publicación") : false;

	if(!body.categories)
		return cb ? cb(false, "Debes enviar las categorias de la publicación") : false;
	
	if(!Array.isArray(body.photos) ||  body.photos.length < 0)
		return cb ? cb(false, "Debes enviar al menos una foto.") : false;
	
	if(!ObjectID.isValid(body.user))
		return cb ? cb(false, "Debes enviar el usuario de esta publicación") : false;
	
	if(Array.isArray(body.localities)){
		let locs = [];
		for(let key in body.localities)
			if(ObjectID.isValid(body.localities[key]))
				locs.push(new ObjectID(body.localities[key]));
		body.localities = locs;
	}else if(ObjectID.isValid(body.localities))
		body.localities = [new ObjectID(body.localities)];
	
	if(Array.isArray(body.categories)){
		for(let key in body.categories)
			if(ObjectID.isValid(body.categories[key]))
				body.categories[key] = new ObjectID(body.categories[key]);
		body.categories = body.categories;
	}else if(ObjectID.isValid(body.categories))
		body.categories = [new ObjectID(body.categories)];
	
	
	var data = {
		name: body.name,
		searchname: this.clean(body.name),
		body: body.body,
		searchbody: this.clean(body.body),
		stock: parseInt(body.stock),
		price: parseFloat(body.price),
		waranty: body.waranty == 'true',
		invoice: body.invoice == 'true',
		physical: body.physical == 'true',
		onfacebook: body.onfacebook == 'true',
		ontwitter: body.ontwitter == 'true',
		onfanpage: body.onfanpage == 'true',
		startdate: body.startdate,
		localities: body.localities,
		categories: body.categories,
		photos: body.photos,
		state: parseInt(body.state),
		user: new ObjectID(body.user),
		added: Date.now(),
		lastupdate: Date.now(),
		sales: 1,
		status: 1,
		miniid: hashids.encode(Date.now()),
		position: {
			type : "Point",
			coordinates : [parseFloat(body.lng), parseFloat(body.lat)]
		}
	};	
	
	return User.findOne({_id: data.user}, function(success, user){
		if(success){
			return Category.validateArray(data.categories, function(success, mes){
				if(success)
					return Locality.validateArray(data.localities, function(success, mes){
						if(success)
							return collection.insert(data, function(err, result){
								if(err){
									this.deleteFiles(data.photos);
									return cb ? cb(false, "Error al agregar publicación") : false;
								}else{
									return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
								}
							}.bind(this));
						else{
							this.deleteFiles(data.photos);
							return cb ? cb(false, mes) : false;
						}
					}.bind(this));
				else{
					this.deleteFiles(data.photos);
					return cb ? cb(false, mes) : false;
				}
			}.bind(this));
		}else{
			this.deleteFiles(data.photos);
			return cb ? cb(false, user) : false;
		}
	}.bind(this));
};

module.exports = Publication;



var	User = require(__dirname +'/user'),
	Category = require(__dirname +'/category'),
	View = require(__dirname +'/view'),
	Transaction = require(__dirname +'/transaction'),
	Like = require(__dirname +'/like'),
	Favorite = require(__dirname +'/favorite'),
	Locality = require(__dirname +'/locality');
	
	User = new User();
	Category = new Category();
	View = new View();
	Transaction = new Transaction();
	Like = new Like();
	Favorite = new Favorite();
	Locality = new Locality();