var db = require('../utils/db'),
	collection = db.collection("banks"),
	ObjectID = require('mongoskin').ObjectID,
	method = Bank.prototype;

function Bank(){	
}

method.doUpdate = function(id, body, cb){
	var data = {};
	
	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(id);
	
	if(body.name)
		data.name = body.name;
		
	if(body.format)
		data.format = body.format;
		
	if(Object.keys(data).length > 0){
		return this.update({_id: id}, {"$set":data}, {}, function(success, mes){
			if(success)
				return cb ? cb(true) : false;
			else
				return cb ? cb(false, mes) : false;
		});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;	
};

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doRemove = function(id){
	id = new ObjectID(id);
	Bankaccount.remove({bank: id});
};

method.remove = function(filter, cb){
	this.find(filter, function(success, items){
		if(success){
			return collection.remove(filter, function(err){
				if(err){
					while(items.length>0)
						this.doRemove(items.pop()._id);
					return cb ? cb(false, "Error al eliminar") : false;
				}else
					return cb ? cb(true) : false;
			}.bind(this));
		}else
			return cb ? cb(false, "Nada for eliminar") : false;
	}.bind(this));
};

method.count = function(filter, cb){
	
	
	if(filter.name)
		filter.name = {"$regex":new RegExp(filter.name,'i')};
		
	
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	
	if(filter.name)
		filter.name = {"$regex":new RegExp(filter.name,'i')};
		
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false) : false;
		}else if(users)
			return cb ? cb(true, users) : false;
		else
			return cb ? cb(false, "Banco no encontrado") : false;
	});
};

method.findOne = function(filter, cb){
	if(filter.name){
		filter.searchName = {"$regex":new RegExp(this.clean(filter.name),'i')};
		delete filter.name;
	}
	
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Banco no encontrada") : false;
	});
};

method.insert = function(body, cb){
	if(!body.name)
		return cb ? cb(false, "Debes enviar todos los campos") : false;
	
	if(!body.format)
		return cb ? cb(false, "Debes enviar el formato de la cuenta") : false;
	
	var data = {
		name : body.name,
		format: body.format
	};
	
	return collection.insert(data, function(err, result){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al insertar") : false;
		}else{
			return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
		}
	});
};

module.exports = Bank;

var Bankaccount = require(__dirname +'/bankaccount');
	Bankaccount = new Bankaccount();