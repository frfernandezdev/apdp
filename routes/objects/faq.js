var db = require('../utils/db'),
	collection = db.collection("faqs"),
	ObjectID = require('mongoskin').ObjectID,
	method = Faq.prototype,
	CategoryFaq = require(__dirname +'/categoryfaq');
	
	CategoryFaq = new CategoryFaq();

function Faq(){
	
}

method.update = function(query, order, extra, cb){
	collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.checkCategories = function(data, id, cb, oldphotos){
	if(data.categoriesfaq){
		return CategoryFaq.validateArray(data.categoriesfaq, function(success, mes){
			if(success){
				return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
					if(success){
						return cb ? cb(true) : false;
					}else{
						return cb ? cb(false, mes) : false;
					}
				}.bind(this));
			}else{
				return cb ? cb(false, mes) : false;
			}
		}.bind(this));
	}else
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				return cb ? cb(true) : false;
			}else{
				return cb ? cb(false, mes) : false;
			}
		}.bind(this));
};


method.doUpdate = function(user, id, body, cb){
	var data = {};
	
	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(id);
	
	if(body.name && body.name.length > 10 && body.name.length < 110){
		data.name = body.name;
		data.searchname = this.clean(body.name);
	}
	
	if(body.body)
		data.body = body.body;
	
	if(body.tags)
		data.tags = this.clean(body.tags);
	
	if(Array.isArray(body.categoriesfaq) && body.categoriesfaq.length > 0)
		data.categoriesfaq = body.categoriesfaq;
		
	if(Object.keys(data).length > 0){
		return this.checkCategories(data, id, cb);
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;	
};

method.deleteFiles = function(files){
	for(let j = 0; j < files.length ; j++)
		fileUtils.deleteFile("routes/uploads/husk/"+files[j]);
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	filter = this.prepareFind(filter);
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};
	

method.prepareFind = function(body){
	var find = {};
	
	if(ObjectID.isValid(body._id))
		find._id = new ObjectID(body._id);
	
	if(body.name)
		find.searchname = {"$regex":new RegExp(this.clean(body.name),'i')};
		
	if(body.body)
		find.searchbody = {"$regex":new RegExp(this.clean(body.body),'i')};
		
	if(body.categoriesfaq && Array.isArray(body.categoriesfaq)){
		find.categoriesfaq = {"$in":body.categoriesfaq};
	}
	
	if(Array.isArray(body.categoriesfaq)){
		let cats = [];
		for(let key in body.categoriesfaq)
			if(ObjectID.isValid(body.categoriesfaq[key]))
				cats.push(new ObjectID(body.categoriesfaq[key]));
		find.categoriesfaq = body.categoriesfaq;
	}else if(ObjectID.isValid(body.categoriesfaq))
		find.categoriesfaq = [new ObjectID(body.categoriesfaq)];
	
	return find;
};


method.find = function(body, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var find = this.prepareFind(body),
		promise = collection.find(find);
		
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, faqs){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(faqs) return cb ? cb(true, faqs) : false;
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(body, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var find = this.prepareFind(body),
		promise = collection.find(find);
		
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, faqs){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(faqs){
			var il = faqs.length, round = 1, i = 0, total = 0, newfaqs = [];
			var handleLoop = function(i){
				this.getFull(faqs[i]._id, function(success, user){
					if(success)
						newfaqs.push(user);
					else
						newfaqs.push(faqs[i]);
					
					if(round == il)
						return cb ? cb(true, newfaqs) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, faqs) : false;
		}
		else return cb ? cb(false, "Publicaciones no encontradas") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	console.log(item);
	//return Locality.validateArray(item.localities, function(success, localities){
		//if(success)
			//item.localities = localities;
			
		return CategoryFaq.validateArray(item.categoriesfaq, function(success, categoriesfaq){
			if(success)
				item.categoriesfaq = categoriesfaq;
				
			return cb ? cb(true, item) : false;
		});
	//});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, user){
			if(success){
				return this.proccess(user, cb);
			}else
				return cb ? cb(false, "Publicación no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.str_replace = function($f, $r, $s){
    return $s.replace(new RegExp("(" + (typeof($f) == "string" ? $f.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&") : $f.map(function(i){return i.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&")}).join("|")) + ")", "g"), typeof($r) == "string" ? $r : typeof($f) == "string" ? $r[0] : function(i){ return $r[$f.indexOf(i)]});
};

method.clean = function(name){
	var f=[' ','Š','Œ','Ž','š','œ','ž','Ÿ','¥','µ','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','?','Ì','Í','Î','Ï','I','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','?','ì','í','î','ï','i','ð','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ'],
		r=['','S','O','Z','s','o','z','Y','Y','u','A','A','A','A','A','A','A','C','E','E','E','E','E','I','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','a','c','e','e','e','e','e','i','i','i','i','i','o','n','o','o','o','o','o','o','u','u','u','u','y','y'];
	return this.str_replace(f,r,name).toLowerCase();		
};

method.dateToTime = function(date){
	date=date.split("-");
	return new Date(date[2]+"-"+date[1]+"-"+date[0]).getTime();
}

method.insert = function(body, cb){
	if(!body.name)
		return cb ? cb(false, "Debes enviar el nombre de la publicación") : false;
		
	if(!body.body)
		return cb ? cb(false, "Debes enviar la descripción de la publicación") : false;
	
	if(!body.tags)
		return cb ? cb(false, "Debes enviar las palabras claves de este FAQ") : false;
		
	if(!Array.isArray(body.categoriesfaq) || body.categoriesfaq.length < 0)
		return cb ? cb(false, "Debes enviar las categorias de la publicación") : false;
		
	var data = {
		name: body.name,
		searchname: this.clean(body.name),
		body: body.body,
		searchbody: this.clean(body.body),
		tags: this.clean(body.tags),
		categoriesfaq: body.categoriesfaq
	};	
		
	
		
	return CategoryFaq.validateArray(data.categoriesfaq, function(success, mes){
		if(success)
			return collection.insert(data, function(err, result){
				if(err){
					console.log(err);
					return cb ? cb(false, "Error al agregar publicación") : false;
				}else{
					return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
				}
			});
		else
			return cb ? cb(false, mes) : false;
	});
		
};

module.exports = Faq;