var sanitizeHtml = require('sanitize-html');


var cleanBody = function(body){
	for(var attr in body) {
		if (Object.prototype.hasOwnProperty.call(body, attr)) {
		
			if(typeof body[attr] == "object")
				body[attr] = cleanBody(body[attr]);
				
			else if(Array.isArray(body[attr]))
				for(let i = 0 ; i < body[attr].length; i++)
					body[attr][i] =  cleanBody(body[attr][i]);
			else
				body[attr] = sanitizeHtml(body[attr], {
				  allowedTags: [],
				  allowedAttributes: []
				});
		}
    }
	return body;
};


var sanetize = function(options){
	var allowedTags = [],
		allowedAttributes = [];
		
		
	if(options && options.allowedTags)
		allowedTags = options.allowedTags;
		
	if(options && options.allowedAttributes)
		allowedAttributes = options.allowedAttributes;
	

	return function(req, res, next){
		for(var attr in req.body) {
			if (Object.prototype.hasOwnProperty.call(req.body, attr)) {
			
				if(typeof req.body[attr] == "object")
					req.body[attr] = cleanBody(req.body[attr]);
					
				else if(Array.isArray(req.body[attr]))
					for(let i = 0 ; i < req.body[attr].length; i++)
						req.body[attr][i] =  cleanBody(req.body[attr][i]);
				else
					req.body[attr] = sanitizeHtml(req.body[attr], {
					  allowedTags: allowedTags,
					  allowedAttributes: allowedAttributes
					});
			}
		}
		
		for(var attr in req.query) {
			if (Object.prototype.hasOwnProperty.call(req.query, attr)) {
			
				if(typeof req.query[attr] == "object")
					req.query[attr] = cleanBody(req.query[attr]);
					
				else if(Array.isArray(req.query[attr]))
					for(let i = 0 ; i < req.query[attr].length; i++)
						req.query[attr][i] =  cleanBody(req.query[attr][i]);
				else
					req.query[attr] = sanitizeHtml(req.query[attr], {
					  allowedTags: allowedTags,
					  allowedAttributes: allowedAttributes
					});
			}
		}
		return next();
	};
};

module.exports = sanetize;