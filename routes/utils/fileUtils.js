var fs = require('fs');
var request = require('request');

function saveBase64Files(files, callback){
	var fl = files.length, i = 0, validFiles = [], failed = false, file, name, cicle = 0;
	function saveCallback(file, index){
		cicle++;       
		if(failed && file){
			deleteFile("routes/uploads/husk/"+file);
			return;
		}

		if(file){
			validFiles[index] = file;
		}else{
			failed = true;
			while(validFiles.length > 0)
				deleteFile("routes/uploads/husk/"+validFiles.pop());
			return callback(false);
		}
		
		if(fl == cicle && !failed){
			console.log(validFiles);
			callback(validFiles);
		}
	}
	
	function doTheThing(file, name, index){
		fs.writeFile("routes/uploads/husk/"+name, file, 'binary', function(err) {
			if(!err){
				saveCallback(name, index);
			}else
				saveCallback(false);
		});
	}
	
	if(fl){
		for(;i<fl;i++){
			file = files[i];
			file = file.replace(/^data:([A-Za-z-+/]+);base64,/, '');
			file = new Buffer(file, 'base64').toString('binary');
			name = Date.now()+"_"+i+"_decodedimage.png";
			doTheThing(file, name, i);
		}
	}else{
		callback(false);
	}
}

function saveFile(files,mime,callback){
		var validMimes = [];
		switch(mime){
			case "image":
				validMimes	=	["image/jpeg","image/png"];
				break;
			default:
				return false;
				break;
		}
		
		var fl = files.length, i = 0, validFiles = [], failed = false, file, name, cicle = 0;
		function saveCallback(file, index){
            cicle++;       
			if(failed && file){
               	deleteFile("routes/uploads/husk/"+file);
				return;
			}

			if(file){
				validFiles[index] = file;
			}else{
				failed = true;
				while(validFiles.length > 0)
					deleteFile("routes/uploads/husk/"+validFiles.pop());
				return callback(false);
			}
			
        	if(fl == cicle && !failed)
				callback(validFiles);
		}
		if(fl){
            for(;i<fl;i++){
        		file = files[i];
				if(validMimes.indexOf(file.mimetype)>=0){
					name = Date.now()+"_"+i+"_"+encodeURIComponent(file.originalname);
					moveFile(file.path,"routes/uploads/husk/"+name,name, saveCallback, i);
				}else{
					saveCallback(false)
					break;
				}
			}
		}else{
			callback(false);
		}
	}

function moveFile(oldPath, newPath, name, callback, i){
	var source = fs.createReadStream(oldPath);
	var dest = fs.createWriteStream(newPath);
	function onEnd(){
		callback(name, i);
		deleteFile(oldPath);
	}
	
	function onError(err){
		console.log(err);
		callback(false, i);
		deleteFile(oldPath);
	}
	

	source.pipe(dest);
	source.on('end', onEnd);
	source.on('error', onError);
}
	
function deleteFile(path){
	fs.exists(path,function(exists){
		if(exists){
			fs.unlink(path);
		}else{
		}
	
	});
	
	}

function getExtension(fileName){
     return fileName.split('.').pop().split(/\#|\?/)[0];
}

function getName(fileName){
     return fileName.split('/').pop().split(/\#|\?/)[0];
}

function downloadFile(uri, filename, name, callback){
  request.head(uri, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);
    request(uri).pipe(fs.createWriteStream(filename)).on('close', function(){callback(name);});
  });
};

function downloadAndSave(files,mime,callback) {
        
        
    var validMimes = [];
	switch(mime){
		case "image":
			validMimes	=	["jpg","jpeg","png"];
			break;
		default:
			return false;
			break;
	}
		
	var fl = files.length, i = 0, validFiles = [], file, name, cicle = 0;
	
    
    function saveCallback(){
        cicle++;
		if(arguments[0]){
        	validFiles.push(arguments[0]);
		}
		if(fl == cicle)
			callback(validFiles);
	}    
    	
	if(fl){
        for(;i<fl;i++){
			file = files[i];
            var ext = getExtension(file);
			if(validMimes.indexOf(ext)>=0){
				name = Date.now()+"_"+encodeURIComponent(getName(file));
				downloadFile(file,"routes/uploads/husk/"+name,name,saveCallback);
			}else{
				saveCallback(false);
				break;
			}
		}
	}else{
		callback(false);
	}
}

//downloadAndSave(['http://wp.patheos.com.s3.amazonaws.com/blogs/faithwalkers/files/2013/03/bigstock-Test-word-on-white-keyboard-27134336.jpg'],'image',function(){console.log('hola');});

var exports = {
	saveFile: saveFile,
	deleteFile: deleteFile,
	moveFile:	moveFile,
	downloadAndSave: downloadAndSave,
	saveBase64Files: saveBase64Files

}

module.exports = exports;
