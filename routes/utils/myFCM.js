var FCM = require('fcm').FCM;
var fcm = new FCM("AIzaSyA7II1d9p3P8nEjYs38c_eUN2mcTATW2vM");


var send = function(devices,payload,cb){
	var message = payload;
	for(var i = 0 ; i < devices.length ; i++){
		message.registration_id = devices[i];

		fcm.send(message, function(err, messageId){
			if (err) {
				console.log(err);
				if(cb)
					cb(false)
			} else {
				if(cb)
					cb(messageId);
			}
		});
	
	}
	
};
var exports = {
	send: send
};
module.exports = exports;