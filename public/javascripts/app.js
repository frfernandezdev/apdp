var app = angular.module('app',
	[
		'angularSpinner',
		'angular-loading-bar',
		'angularUtils.directives.dirPagination',
		'ui.router',
		'yaru22.angular-timeago',
		'angucomplete-alt',
		'bw.paging',
		'720kb.socialshare',
		'ngCookies',
		'ngFileUpload',
		'timer',
		'ngAnimate',
		'duScroll',
		'toastr',
		'multipleSelect',
		'ngMap',
		'ngResource',
		'ngSanitize',
		'ui.carousel',
		'oitozero.ngSweetAlert',
		'ui.bootstrap',
		'ngCroppie',
		'textAngular'
		// 'ngImgCrop'
		// 'mdl',
		// 'infinite-scroll',
		// 'ja.qr'
	]
);

app.config(function($locationProvider) {
	$locationProvider.html5Mode({
		enabled: true
	});

	// timeAgoSettings.allowFuture = true;
});


app.config(['$provide', function ($provide) {
    $provide.decorator('$locale', ['$delegate', function ($delegate) {
        $delegate.NUMBER_FORMATS.DECIMAL_SEP = ',';
        return $delegate;
    }]);
}]);

// app.run(function($rootScope, $anchorScroll, $timeout){
// 	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
// 		$timeout(function(){
// 			$('.modal.fade').click();
// 			$('.modal-backdrop').hide();
// 		},100);
// 		$anchorScroll();
// 	});
//     $rootScope.$on('$viewContentLoaded', function(event, next) {
//         componentHandler.upgradeAllRegistered();
// 	});
// });

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
    	.state('base', {
    		abstract: true,
    		templateUrl: 'views/layout',
    		controller: LayoutController
    	})
				.state('base.index', {
					url: '/',
					title: 'APrecioDePana Venezuela - Compra y vende lo que quieras',
					templateUrl: 'views/index',
					controller: IndexController
				})
				.state('base.search',{
					url: '/search/:search?page',
					options: 'search',
					title: 'APrecioDePana Venezuela - Compra y vende lo que quieras',
					templateUrl: 'views/search',
					controller: SearchController
				})
				.state('base.list', {
					url: '/list?page',
					options: 'list',
					title: 'APrecioDePana Venezuela - Compra y vende lo que quieras',
					templateUrl: 'views/search',
					controller: SearchController
				})
				.state('base.searchCategories', {
					url: '/categories/:id?page',
					options: 'categories',
					title: 'APrecioDePana Venezuela - Compra y vende lo que quieras',
					templateUrl: 'views/search',
					controller: SearchController
				})
				.state('base.publication', {
					url: '/publication/:id',
					title: 'APrecioDePana Venezuela - Compra y vende lo que quieras',
					templateUrl: 'views/publication',
					controller: PublicationController
				})
				.state('publish',{
					parent: 'base',
					title: 'Publicar Gratis, Vende sin Complicaciones',
					templateUrl: 'views/publish',
					controller: PublishController
				})
					.state('publish.base',{
						url: '/publish',
						title: 'Publicar Gratis, Vende sin Complicaciones',
						templateUrl: 'views/publish-base',
						controller: PublishBaseController
					})
					.state('publish.one',{
						url: '/publish/one/:selector',
						params: {
							selector: { array: true }
						},
						title: 'Publicar Gratis, Vende sin Complicaciones',
						templateUrl: 'views/publish-one',
						controller: PublishOneController

					})
					.state('publish.two',{
						url: '/publish/two/:selector',
						params: {
							selector: { array: true }
						},
						title: 'Publicar Gratis, Vende sin Complicaciones',
						templateUrl: 'views/publish-two',
						controller: PublishTwoController
					})
					.state('publish.third',{
						url: '/publish/third/:selector',
						params: {
							selector: { array: true }
						},
						title: 'Publicar Gratis, Vende sin Complicaciones',
						templateUrl: 'views/publish-third',
						controller: PublishThirdController
					})
				.state('user', {
					url: '/user/:id',
					parent: 'base',
					templateUrl: 'views/profile',
					controller: UserController
				})
					.state('user.publication', {
						url: '/publication?page',
						templateUrl: 'views/profile_publications',
						controller: ProfilePublicationController
					})
					.state('user.info', {
						url: '/info',
						templateUrl: 'views/profile_info',
						controller: ProfileInfoController
					})
					.state('user.friends', {
						url: '/friends?page',
						templateUrl: 'views/profile_friends',
						controller: ProfileFriendController
					})
					.state('user.following', {
						url: '/following?page&search',
						templateUrl: 'views/profile_following',
						controller: ProfileFollowingController
					})
					.state('user.followers', {
						url: '/followers?page',
						templateUrl: 'views/profile_followers',
						controller: ProfileFollowerController
					})
				.state('profile', {
					url: '/profile',
					parent: 'base',
					templateUrl: 'views/profile',
					controller: ProfileController
				})
					.state('profile.publication', {
						url: '/publication?page',
						templateUrl: 'views/profile_publications',
						controller: ProfilePublicationController
					})
					.state('profile.info', {
						url: '/info',
						templateUrl: 'views/profile_info',
						controller: ProfileInfoController
					})
					.state('profile.friends', {
						url: '/friends?page',
						templateUrl: 'views/profile_friends',
						controller: ProfileFriendController
					})
					.state('profile.following', {
						url: '/following?page',
						templateUrl: 'views/profile_following',
						controller: ProfileFollowingController
					})
					.state('profile.followers', {
						url: '/followers?page',
						templateUrl: 'views/profile_followers',
						controller: ProfileFollowerController
					})
					.state('profile.publicationLike',{
						url: '/publicationLike?page',
						templateUrl: 'views/publicationLikes',
						controller: PublicationLikes
					})
				.state('summary', {
					parent: 'base',
					templateUrl: 'views/summary'
				})
					.state('summary.summary', {
						url: '/summary',
						title: 'Resumen',
						templateUrl: 'views/summary-summary',
						controller: SummaryController
					})
					.state('summary.reputation', {
						url: '/reputacion',
						title: 'Reputación',
						templateUrl: 'views/summary-reputation',
						controller: ReputationController
					})
					.state('sales',{
						url: '/sales',
						parent: 'summary',
						templateUrl:'views/sales'
					})
						.state('sales.publications', {
							url: '/publications?search&state&page&orderSales&orderDate',
							title: 'Ventas | Mis publicationes',
							templateUrl: 'views/salesPublications',
							controller: salesPublicationsController
						})
						.state('sales.sales', {
							url: '/sales?search&state&page&orderSales&orderDate',
							title: 'Mis Ventas',
							templateUrl: 'views/salesSales',
							controller: salesSalesController
						})
						.state('sales.question', {
							url: '/question',
							title: 'Ventas | Preguntas',
							templateUrl: 'views/salesQuestion',
							controller: salesQuestionController
						})
						.state('sales.claims', {
							url: '/claims',
							title: 'Ventas | Reclamos',
							templateUrl: 'views/salesClaims',
							controller: salesClaimsController
						})
					.state('purchases',{
						url: '/purchases',
						parent: 'summary',
						templateUrl: 'views/purchases'
					})
						.state('purchases.favorites', {
							url: '/favorites?search&page&orderSales&orderDate',
							title: 'Compras | Favoritos',
							templateUrl: 'views/purchasesFavorites',
							controller: purchasesFavoritesController
						})
						.state('purchases.question', {
							url: '/question',
							title: 'Compras | Preguntas',
							templateUrl: 'views/purchasesQuestion',
							controller: purchasesQuestionController
						})
						.state('purchases.purchases', {
							url: '/purchases?search&state&page&orderSales&orderDate',
							title: 'Mis Compras',
							templateUrl: 'views/purchasesPurchases',
							controller: purchasesPurchasesController
						})
					.state('summary.billingPay', {
						url: '/billingPay',
						title: 'Cuentas por Pagar',
						templateUrl: 'views/billingPay',
						controller: billingPayController
					})
					.state('managerNetwork',{
						url: '/managerNetwork',
						parent: 'summary',
						templateUrl: 'views/managerNetwork'
					})
						.state('managerNetwork.link', {
							url: '/Link',
							title: 'Vincular Red Social',
							templateUrl: 'views/managerNetworkLink',
							controller: managerNetworkLinkController
						})
						.state('managerNetwork.publicationAuto', {
							url: '/publicationAuto',
							title: 'Publicaciones automatizadas',
							templateUrl: 'views/managerNetworkPublicationAuto',
							controller: managerNetworkPublicationAutoController
						})
						.state('managerNetwork.campaigns', {
							url: '/campaigns',
							title: 'Campañas publicitarias',
							templateUrl: 'views/managerNetworkCampaigns',
							controller: managerNetworkCampaignsController
						})
					.state('summary.configurationDataUser', {
						url: '/configurationDataUser',
						title: 'Configuracion | Datos Personales',
						templateUrl: 'views/configurationDataUser',
						controller: configurationDataUserController
					})
					.state('base.checkout',{
						url: '/checkout/:publish?type&quantity&id',
						title: 'Compra lo que quieras , rápido y sin complicaciones',
						templateUrl: 'views/checkout',
						controller: checkoutController
					})
					.state('bankcheckout',{
						parent: 'base',
						title: 'Publicar Gratis, Vende sin Complicaciones',
						templateUrl: 'views/bankcheckout',
						controller: bankcheckout
					})
						.state('bankcheckout.bankcheckoutOne',{
							url: '/bankcheckoutOne/:id?type',
							title: 'Compra lo que quieras , rápido y sin complicaciones',
							templateUrl: 'views/bankcheckoutOne',
							controller: bankcheckoutOne
						})
						.state('bankcheckout.bankcheckoutTwo',{
							url: '/bankcheckoutTwo/:id',
							title: 'Compra lo que quieras , rápido y sin complicaciones',
							templateUrl: 'views/bankcheckoutTwo',
							controller: bankcheckoutTwo
						})
						.state('bankcheckout.bankcheckoutThird',{
							url: '/bankcheckoutThird/:id',
							title: 'Compra lo que quieras , rápido y sin complicaciones',
							templateUrl: 'views/bankcheckoutThird',
							controller: bankcheckoutThird
						})
});

app.value('duScrollDuration', 2000).value('duScrollOffset', 125);

app.config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({color: '#fffff',position:'fixed',top:'50%'});
}]);
