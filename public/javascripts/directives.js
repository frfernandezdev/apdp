'use strict';

/* Directives */

app.directive('appVersion', function() {
    return function(scope, elm, attrs) {
      elm.text("hola");
    };
  });

app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('localityCreator', ['Locality', function(Locality){
	return {
		restrict: 'E',
		templateUrl: 'views/directiveLocalityCreator',
		scope: {
			model: "=",
			str : "=?",
			change : "&?"
		},
		link: function(scope, element, attr){

			function loop(i){
				i = parseInt(i);
				scope.Locality.getChilds(scope.model[i], function(success, childs){
					if(success){
						scope.con[i+1] = childs;

						for(var key in scope.con[i])
							if(scope.con[i][key]._id == scope.model[i]){
								scope.str[i] = scope.con[i][key].name;
								break;
							}

					}
				});
			};


			function build(){
				scope.Locality = angular.copy(Locality);
				if(scope.Locality.items.length == 0)
					scope.Locality.s.parent = "false";

				scope.con = [];
				scope.str = [];

				scope.Locality.get(function(){
					scope.con[0] = scope.Locality.items;
					if(Array.isArray(scope.model)){
						for(var i = 0; i < scope.model.length; i++)
							loop(i);
					}else
						scope.model = [];
				});



				scope.childOf = function(index){
					index = parseInt(index);
					if(scope.change) scope.change();

					scope.Locality.getChilds(scope.model[index], function(success, items){

						for(var key in scope.con[index])
							if(scope.con[index][key]._id == scope.model[index]){
								scope.str[index] = scope.con[index][key].name;
								break;
							}

						if(success)
							scope.con[index+1] = angular.copy(items);

						for(var i = 2; i > index+1; i--){
							delete scope.con[i];
							delete scope.str[i];
						}
					});
				};

				scope.$watch('model', function(nv,ov){
					if(Array.isArray(scope.model)){
						for(var i = 0; i < scope.model.length; i++){
							loop(i);
						}
					}
				});
			}


			scope.$on('newLoc', function(){
				build();
			});
			build();
		}
	}
}]);

app.directive('localityPicker', ['Locality', function(Locality){
  return {
    restrict: 'E',
    templateUrl: 'views/directiveLocalityPicker',
    scope: {
      model: "=",
      str : "=?",
      change : "&?"
    },
    link: function(scope, element, attr){

      function loop(i){
        i = parseInt(i);
        scope.Locality.getChilds(scope.model[i], function(success, childs){
        if(success){
          scope.con[i+1] = childs;
          for(var key in scope.con[i])
            if(scope.con[i][key]._id == scope.model[i]){
              scope.str[i] = scope.con[i][key].name;
              break;
            }
          }
        });
      };
      scope.Locality = angular.copy(Locality);
      if(scope.Locality.items.length == 0)
        scope.Locality.s.parent = "false";
      scope.con = [];
      scope.str = [];

      scope.Locality.get(function(){
      scope.con[0] = scope.Locality.items;
      if(Array.isArray(scope.model)){
        for(var i = 0; i < scope.model.length; i++)
          loop(i);
      }else
        scope.model = [];
      });

      scope.childOf = function(index){
        index = parseInt(index);
        if(scope.change) scope.change();

        scope.Locality.getChilds(scope.model[index], function(success, items){
  	      scope.con.splice(index+1);
  	      scope.str.splice(index+1);
  	      scope.model.splice(index+1);
          for(var key in scope.con[index])
            if(scope.con[index][key]._id == scope.model[index]){
              scope.str[index] = scope.con[index][key].name;
              break;
            }

            if(success)
              scope.con[index+1] = angular.copy(items);
        });
      };
      scope.$watch('model', function(nv,ov){
        if(Array.isArray(scope.model)){
          for(var i = 0; i < scope.model.length; i++){
            loop(i);
          }
        }
      });
    }
  }
}]);

app.directive('doubleScroll', function () {

        return {
            restrict: 'A',
            scope: {
                wait: "="
            },
            link: function (scope, el, attrs) {
				scope.$watch('wait', function(newValue, oldValue) {
					if(newValue)
						$(el).doubleScroll();
				}, true);
            }
      };
  });

app.directive('stickyHeader', function () {

        return {
            restrict: 'A',
            scope: {
                wait: "="
            },
            link: function (scope, el, attrs) {
				scope.$watch('wait', function(newValue, oldValue) {
					if(newValue)
						$(el).stickyTableHeaders();
				}, true);
            }
      };
  });

app.directive('btCarousel', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
				$(el).carousel("pause").removeData();
				$(el).carousel(0);
            }
      };
  });

app.directive('timePicker', function () {

        var options = {clear:"Limpiar",
        format: 'HH:i',
        container: 'body'};

        return {
            restrict: 'A',
            scope: {
                tpMax: "=",
                tpMin: "=",
				wait: "="
            },
            link: function (scope, el, attrs) {

				var make = function(){
					if(scope.tpMin)
						options.min = [scope.tpMin,0];
					if(scope.tpMax)
						options.max = [scope.tpMax,59];

					if(scope.wait){
						if(scope.tpMin && scope.tpMax)
							$(el).pickatime(options);
					}else
						$(el).pickatime(options);
				};

				scope.$watch('tpMax', function(newValue, oldValue) {
					make();
				}, true);

				scope.$watch('tpMin', function(newValue, oldValue) {
					make();
				}, true);

				if(!scope.wait)
					make();
            }
      };
  });

app.directive('datePicker', function () {

        var options = {monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        selectYears: 100,
        selectMonths: true,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar',
        format:'dd-mm-yyyy',
        container: 'body'};

        return {
            restrict: 'A',
            scope: {
                max: "=",
                min: "="
            },
            link: function (scope, el, attrs) {
                var today = Date.now();
                if(scope.min)
                    options.min = new Date(today+parseInt(scope.min));
                if(scope.max)
                    options.max = new Date(today+parseInt(scope.max));

                $(el).pickadate(options);
            }
      };
  });

app.directive("whenScrolled", function(){
  return{

    restrict: 'A',
    link: function(scope, elem, attrs){

      // we get a list of elements of size 1 and need the first element
      let raw = elem[0];

      // we load more elements when scrolled past a limit
      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;

        // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});

app.directive("dataCarousel", function(){
  var options = {
    interval: 1000
  }
  return{
    restrict: 'A',
    link: function (scope, el, attrs) {
      $(el).carousel(options)

    }
  }
})

app.directive("autoSearch",['$document','$timeout',function($document,$timeout){
  return{
    restrict: 'A',
    link: function(scope, el, attr){
      let form = angular.element(el),
          ul = angular.element(el.children('ul'));
      form.on('focusout',function(){
        $timeout(function () {
          ul.removeClass('show');
        }, 200);
      })
      form.on('click',function(){
        if(scope.flag==true){
          ul.addClass('show');
        }
      })
    }
  }
}])

app.directive("autoSearchResponsive",['$document','$timeout',function($document,$timeout){
  return{
    restrict: 'A',
    link: function(scope, el, attr){
      let form = angular.element(el.children('form')),
          ul = angular.element(el.children('ul'));
      form.on('focusout',function(){
        $timeout(function () {
          ul.removeClass('show');
        }, 200);
      })
      form.on('click',function(){
        if(scope.flag==true){
          ul.addClass('show');
        }
      })
    }
  }
}])

app.directive("hoverProfile",function(){
  return{
    restrict: 'A',
    link: function(scope, el, attr){
      $(el).hover(
        function(){
          $(el[0].children[0]).addClass('hover')
        },
        function(){
          $(el[0].children[0]).removeClass('hover')
        }
      )
    }
  }
})

app.directive("hoverEdit",['$timeout',function($timeout){
  return{
    restrict: 'AE',
    scope: {
      hoverEdit: '&',
      action: '='
    },
    link: function(scope, el, attr){
      scope.$watch('action',function(data){
        let button = el.children('span').children('button'),
            buttonClass = el[0].children[0].children[0].children[0].classList[1],
            input = el.children('input'),
            buttonClose = el.children('span.btn-close'),
            contentButton = el[0].children[0].children[0].children[0].innerHTML,
            spanErr = el.children('span.help-block');

        var inputVal = input.val();

        let flag = true;

        buttonClass ? buttonClass: el[0].children[0].children[0].children[0].classList[0];

        if(data){
          el.hover(function(){
            if(flag){
              button.children('i').removeClass(buttonClass)
              button.children('i').text('');
              button.children('i').addClass('fa-edit')
            }
            button.prop("disabled", false);
          },function(){
            if(input.val()===inputVal){
              button.children('i').removeClass('fa-edit')
              button.children('i').removeClass('fa-check')
              button.children('i').addClass(buttonClass)
              button.children('i').text(contentButton)
              button.prop("disabled", true);
              input.prop("disabled", true);
              flag = true;
            }else {
              button.children('i').removeClass('fa-edit')
              button.children('i').addClass('fa-check')
              buttonClose.removeClass('hidden')
              input.prop("disabled", false);
            }
          })
        }

        button.on('click',function(){
          if(flag){
            input.prop("disabled", false);
            button.children('i').removeClass('fa-edit')
            button.children('i').addClass('fa-check')
            button.children('i').removeClass(buttonClass)
            button.children('i').text('')
            flag = false;
          }else {
            console.log(input.value)
            if(!input.val()){
              el.addClass('has-error');
              spanErr.show();
            }else {
              el.removeClass('has-error');
              spanErr.hide();
            }

            if(input.val()!=inputVal){
              scope.hoverEdit(function(success){
                if(success){
                  console.log('success')
                }
              });
              flag = true;
              inputVal = input.val();
              Close();
            }
            flag = true;
          }
        })

        buttonClose.on('click',function(){
          flag = true;
          input.val(inputVal);
          Close();
        })

        function Close() {
          buttonClose.addClass('hidden');
          button.prop("disabled", true);
          input.prop("disabled", true);
          button.children('i').removeClass('fa-edit')
          button.children('i').removeClass('fa-check')
          button.children('i').addClass(buttonClass)
          button.children('i').text(contentButton)
        }
      })
    }
  }
}])

app.directive("campoEdit",['$timeout',function($timeout){
  var html ='<div class="about-me"><div class="title"><b></b></div><div class="content"><span class="fa fa-quote-left" style="opacity: 0.9;font-size: 12px;display: inline;"></span><span contenteditable ></span><span class="fa fa-quote-right" style="opacity: 0.9;font-size: 12px;display: inline;"></span><span class="btn fa fa-check"></span></div></div>';
  return{
    restrict: 'AE',
    scope: {
      ngModel: '=',
      on: '&',
      action: '='
    },
    template: html,
    replace: true,
    require: 'ngModel',
    link: function(scope, elem, attr, ngModel){
      let content,
          label,
          title = elem.children('div.title'),
          spanEdit = elem.children('div.content').children('span[contenteditable]'),
          button = elem.children('div.content').children('span.btn.fa');

      var flag = true;

      label = elem.children('div.title').children('b');
      label.text(attr.label)

      scope.$watch('ngModel',function(n,o){
        content = n;

        if(!ngModel.$viewValue)
        content = 'Sin información';

        spanEdit.text(content)
      })

      spanEdit.attr('contenteditable','false')

      scope.$watch('action',function(data){
        if(data){
          elem.hover(function(){
            spanEdit.on('click',function(){
              button.show();
              if(!ngModel.$viewValue)
                spanEdit.text('  ')
              spanEdit.attr('contenteditable','true')
            })
            button.on('click',function(){
              if(ngModel.$viewValue!=spanEdit.text()){
                flag = false;
                ngModel.$setViewValue(spanEdit.text());
                scope.on();
                content = ngModel.$viewValue;
                flag = true;
              }else {
                flag = true;
              }
            })
          },function(){
            if(flag){
              if(ngModel.$viewValue==spanEdit.text()){
                button.hide();
                spanEdit.text(content)
                spanEdit.attr('contenteditable','false')
              }else {

              }
            }
          })
        }
        // Style element
        button.css({
          "padding": "10px 10px",
          "margin-left": "10px",
          "border": "1px solid #ccc",
          "border-radius": "5px",
          "color": "black",
          "float": "right",
          "margin-top": "10px",
          "display": "none"
        })
        title.css({
          "margin-bottom": "10px"
        })
        label.css({
          "color": "black",
          "font-size": "16px"
        })
        spanEdit.css({
          "padding": "0 8px",
          "font-size": "16px",
          "outline": "transparent",
          "border-color": "transparent",
          "word-break": "break-all"
        })
      })
    }
  }
}])

app.directive("btnFollow",['Auth','Follows','$timeout','$rootScope','$document',function(Auth,Follows,$timeout,$rootScope,$document){
  var html = '<button type="button" ng-hide="Auth.isLogged() && Auth.user._id == ngModel" class="btn btn-sm btn-default btn-following"><i class="fa fa-thumbs-up fa-fw"></i><span class="hidden-xs"></span></button>';
  return{
    restrict: 'AE',
    scope: {
      ngModel: '=',
      on: '='
    },
    template: html,
    replace: true,
    require: 'ngModel',
    link: function(scope, el, attr, ngModel) {
      let span   = el.children('span'),
          follow = angular.copy(Follows),
          on     = false;

    	scope.Auth = Auth;

      el.on('click',function(e){
        e.preventDefault();
        if(Auth.isLogged()){
    		  if(on){
    				follow.d._id = ngModel.$viewValue;
    				follow.remove(function(success){
    				  span.text('...Cargando')
    				  if(success){
      					span.text('Seguir')
      					el.removeClass('active')
      					on = false;
    				  }else {
      					span.text('Siguiendo')
      					el.addClass('active')
    					  on = true;
    				  }
    				})
    		  }else {
    				follow.a._id = ngModel.$viewValue;
    				follow.post(function(success){
    				  span.text('...Cargando')
    				  if(success){
      					span.text('Siguiendo')
      					el.addClass('active')
    					  on = true;
    				  }else {
      					span.text('Siguiendo')
      					el.addClass('active')
      					on = true;
    				  }
    			  })
    		  }
    	  }
      })
      scope.$watch('on',function(data){
        if(Auth.isLogged()){
            el.removeClass('btn-disabled')
            if(data){
              span.text('Siguiendo')
              el.addClass('active')
              on = true;
            }else {
              span.text('Seguir')
              el.removeClass('active')
            }
        }else {
          span.text('Seguir');
        }
      })
    }
  }
}])

// app.directive("zoomImg",function(){
//   var html = '<div class="content-img" style="width: 100%;height: 100%;">'+
//                 '<img ng-src="http://50.21.179.35:8083/{{img}}" style="width: 100%;height: 100%;"/>'+
//              '</div>',
//       htmlOut = '<div id="ImgZoom" style="position: absolute;top: 0px;right: -500px;width: 500px;height:500px;">'+
//                   '<div class="overflow" style="overflow: hidden;">'+
//                     '<img ng-src="http://50.21.179.35:8083/{{img}}" style="width: 100%;height: 100%;"/>'+
//                   '</div>'+
//                 '</div>';
//   return{
//     restrict: 'AE',
//     scope: {
//       ngModel: '='
//     },
//     replace: true,
//     require: 'ngModel',
//     template: html,
//     link: function(scope, el, attr, ngModel){
//       scope.$watch(function(){
//         console.log(el);
//
//         scope.img = ngModel.$viewValue;
//
//         let PositionImg = el.children('img').offset();
//
//         el.hover(function(){
//           $('.carousel.slide').append(htmlOut);
//           console.log($('.carousel.slide').is('#ImgZoom'));
//           el.mousemove(function(ev){
//               console.log(ev.offsetX+' - '+ev.offsetY)
//           })
//         },function(){
//           $('.carousel.slide').remove('#ImgZoom');
//           console.log($('.carousel.slide').is('#ImgZoom'));
//         })
//       })
//     }
//   }
// })

app.directive("btnLike",['Likes',function(Likes){
  var html = '<a href="#" style="">'+
                '<i class="fa fa-heart fa-fw"></i>'
              '</a>';
  return {
    restrict: 'AE',
    scope: {
      id: '=',
      like: '=',
      change: '=',
    },
    replace: true,
    template: html,
    link: function(scope, el, attr){
      scope.Likes = angular.copy(Likes);

      scope.$watch('id',function(n,o){
        scope.id = n;
      })
      scope.$watch('like',function(n,o){
        scope.like = n;
        if(scope.like == 1)
          el.children('i.fa.fa-heart').addClass('red');
        else
          el.children('i.fa.fa-heart').removeClass('red');
      })

      $(el).on('click',function(e){
        e.preventDefault();
        if(scope.like == 0){
          scope.Likes.a.publication = scope.id;
          scope.Likes.post(function(success){
            if(success)
              scope.like = 1;
          })
        }
        else {
          scope.Likes.s = {
            byMe: true,
            publication: scope.id
          }
          scope.Likes.get(function(success){
            if(success){
              console.log(scope.Likes.items)
              console.log(scope.Likes.items[0]._id)
              scope.Likes.d._id = scope.Likes.items[0]._id;
              scope.Likes.remove(function(success){
                if(success)
                  scope.like = 0;
                  scope.change();
              })
            }
          })
        }
      })
    }
  }
}])

app.directive("accordion",function(){
  return{
    restrict: 'AE',
    link: function(scope, el, attr) {
      $(el).dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
        cookie: 'dcjq-accordion',
        classExpand: 'dcjq-current-parent'
      });
    }
  }
})

app.filter("filterMoney",function(){
  return function(input , symbol, place) {

    if(isNaN(input))
      return input;
    else
      var symbol = symbol || '$';
      var place  = place === undefined ? true :place;

      var number = (parseFloat(input) || 0).toFixed(2)+"";

      var parts = number.split(/[-.]/);

      input = parts[parts.length > 1 ? parts.length - 2 : 0];
      var result = input.substr(input.length - 3, 3) + (parts.length > 1 ? ',' + parts[parts.length - 1] : '');

      var start = input.length - 6;
      var idx = 0;

      while (start > -3) {
          result = (start > 0 ? input.substr(start, 3) : input.substr(0, 3 + start))
              + '.' + result;
          start -= 3;
      }
      if(place === true)
        return symbol+ '  ' + result;
      else
        return result + '  ' + symbol;
  }
})

app.directive('multipleSelect',['$document',function($document){
  var html =  '<select class="multipleSelect form-control" multiple>'+
                '<option value="Do">'+
                  'Domingo'+
                '</option>'+
                '<option value="Lu">'+
                  'Lunes'+
                '</option>'+
                '<option value="Ma">'+
                  'Martes'+
                '</option>'+
                '<option value="Mi">'+
                  'Miercoles'+
                '</option>'+
                '<option value="Ju">'+
                  'Jueves'+
                '</option>'+
                '<option value="Vi">'+
                  'Viernes'+
                '</option>'+
                '<option value="Sa">'+
                  'Sabado'+
                '</option>'+
              '</select>';
  return{
    restrict: 'AE',
    scope: {
      ngModel: '=',
    },
    template: html,
    replace: true,
    require: 'ngModel',
    link: function(scope, el, attr, ngModel){
      console.log(scope)
      console.log(el)
      console.log(attr)
      console.log(ngModel)

      let select = el;

      el.children('option').bind('click',function(){
        // $(this).attr('selected','selected');
        console.log('selected')
      })
      ngModel.$setViewValue(el.val());
    }
  }
}])

app.directive('navbarAutoclose',['$document','$timeout',function($document,$timeout){
  return {
    restrict: 'AE',
    link: function(scope, elem, attr){
      let $window = $(window).width(),
          el = angular.element(elem);

      if($window < 764){
        el.on('click',function(){
          el.collapse('hide')
        })
      }

    }
  }
}])
