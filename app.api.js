var express = require('express'),
	compression = require('compression'),
	minify = require('express-minify'),
	cors = require('cors'),
	path = require('path'),
	logger = require('morgan'),
	ObjectID = require('mongoskin').ObjectID,
	cookieParser = require('cookie-parser'),
	autoauth = require('./routes/middlewares/autoauth'),
	bodyParser = require('body-parser'),
	User = require('./routes/objects/user'),
	mailer = require('./routes/utils/mailer'),
	Valuepair = require('./routes/objects/valuepair'),
	Locality = require('./routes/objects/locality');
	
	
	User = new User();
	Valuepair = new Valuepair();
	Locality = new Locality();
	/*
	Locality.insert({name:"Venezuela"}, function(a,b){
		console.log(a,b);
	});*/
	//User.update({},{"$set":{online:false}},{multi:true});
	//User.update({},{"$set":{"rating.total":4.5}},{multi:true});
	//Valuepair.insert({key:"termb", value:"content"});
	//Valuepair.insert({key:"termc", value:"content"});
	//Valuepair.insert({key:"privacy", value:"content"});
	/*User.insert({username: "gabomartz", 
		name: "Gabriel",
		photo: "photo.jpg",
		lastname: "Martinez",
		document: "21417211",
		phone: "+584143711248",
		address: "Carrera 12",
		mail: "gabomartz@gmail.com",
		localities: new ObjectID("599cb26f1f1fbca941186245"),
		dob: "09-10-1992",
		gender: 1,
		password: "09101992",
		cover: "cover.jpg",
		priv: 1
		}, function(a, b){console.log(a,b);});*/
	
var app = express();
var corsOptions = {
	origin: ["*"],
	optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(compression());
//app.use(cors(corsOptions));
app.use(cors());
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));


//app.options('*', cors(corsOptions), function(req, res, next){console.log("here");next();});

var auth = require('./routes/api/auth'),
	bankaccounts = require('./routes/api/bankaccounts'),
	banks = require('./routes/api/banks'),
	banners = require('./routes/api/banners'),
	categories = require('./routes/api/categories'),
	categoriesfaq = require('./routes/api/categoriesfaq'),
	chats = require('./routes/api/chats'),
	comments = require('./routes/api/comments'),
	faqs = require('./routes/api/faqs'),
	favorites = require('./routes/api/favorites'),
	follow = require('./routes/api/follows'),
	friendrequests = require('./routes/api/friendrequests'),
	friendships = require('./routes/api/friendships'),
	likes = require('./routes/api/likes'),
	localities = require('./routes/api/localities'),
	messages = require('./routes/api/messages'),
	notifications = require('./routes/api/notifications'),
	publications = require('./routes/api/publications'),
	reportcomments = require('./routes/api/reportcomments'),
	reports = require('./routes/api/reports'),
	reviews = require('./routes/api/reviews'),
	spots = require('./routes/api/spots'),
	transactions = require('./routes/api/transactions'),
	users = require('./routes/api/users'),
	valuepairs = require('./routes/api/valuepairs'),
	views = require('./routes/api/views');

app.use(cookieParser());	
app.use(autoauth);
app.get('/*', function (req, res, next) {
  res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  res.setHeader("Pragma", "no-cache");
  res.setHeader("Expires", "0");
  next();
});	
app.use('/auth', auth);
app.use('/bankaccounts', bankaccounts);
app.use('/banks', banks);
app.use('/banners', banners);
app.use('/categories', categories);
app.use('/categoriesfaq', categoriesfaq);
app.use('/chats', chats);
app.use('/comments', comments);
app.use('/faqs', faqs);
app.use('/favorites', favorites);
app.use('/follows', follow);
app.use('/friendrequests', friendrequests);
app.use('/friendships', friendships);
app.use('/likes', likes);
app.use('/localities', localities);
app.use('/messages', messages);
app.use('/notifications', notifications);
app.use('/publications', publications);
app.use('/reportcomments', reportcomments);
app.use('/reports', reports);
app.use('/reviews', reviews);
app.use('/spots', spots);
app.use('/transactions', transactions);
app.use('/users', users);
app.use('/valuepairs', valuepairs);
app.use('/views', views);
/*app.use('/contact',function(req, res){
	var body = req.body,
		mailOptions = {
			from: '"Delivery en Bahia" <contacto@deliveryenbahia.com>',
			replyTo: '"'+body.name+'" <'+body.mail+'>',
			to: 'info@nixel.com.ar',
			encoding : 'utf-8',
			subject: body.subject,
			text: 'Nuevo mail de:\nNombre: '+body.name+'\nEmail: '+body.mail+'\nTeléfono: '+body.phone+'\nAsunto: '+body.subject+'\nMensaje: '+body.body,
			html: 'Nuevo mail de:<br>Nombre: <b>'+body.name+'</b><br>Email: <b>'+body.mail+'</b><br>Teléfono: <b>'+body.phone+'</b><br>Asunto: <b>'+body.subject+'</b><br>Mensaje: <b>'+body.body+'</b>'
		};	
		
	mailer.sendMail(mailOptions);
	return res.json({e:0});

});*/

/*
setInterval(function() {
	User.update({expires: {"$lte": Date.now()}},
		{"$set":{plan:1}, "$push": {
			categories: {
				each: [0],
				slice: 1
			}
		}},
		{multi: true});
}, 86400000);
*/

setInterval(function() {
	User.update({},
		{"$pull":{bitacora:{"$or":[{"bitacora.active": false},{"bitacora.expires":{"$lt":Date.now()}}]}}},
		{multi: true});
}, 1000);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  console.log(err);
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

module.exports = app;
